/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QSplitter>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>

#include "mainwindow.h"
#include "editmaster.h"
#include "blocklistitem.h"
#include "blockdialog.h"
#include "project.h"

#include "blockspanel.h"

BlocksPanel::BlocksPanel(Context* context, QWidget *parent) : QWidget(parent), Object(context),
    blocksetList_{nullptr},
    blockList_{nullptr},
    addBlocksetButton_{nullptr},
    deleteBlocksetButton_{nullptr},
    previousBlocksetButton_{nullptr},
    nextBlocksetButton_{nullptr},
    addBlockButton_{nullptr},
    editBlockButton_{nullptr},
    removeBlockButton_{nullptr},
    zoomSlider_{nullptr},
    rotationSlider_{nullptr}
{
    QVBoxLayout* mainLayout{ new QVBoxLayout() };
    mainLayout->setMargin(2);
    QSplitter* splitter{ new QSplitter(this) };
    splitter->setOrientation(Qt::Vertical);

    //Construct top half
    QWidget* topHalf{ new QWidget() };
    QVBoxLayout* topLayout{ new QVBoxLayout() };
    topLayout->setMargin(0);

    QHBoxLayout* topSetsRow{ new QHBoxLayout() };
    addBlocksetButton_    = new QPushButton("Add set");
    deleteBlocksetButton_ = new QPushButton("Delete set");
    topSetsRow->addWidget(addBlocksetButton_);
    topSetsRow->addWidget(deleteBlocksetButton_);
    topLayout->addLayout(topSetsRow);

    blocksetList_ = new QListWidget();
    blocksetList_->setSortingEnabled(true);
    blocksetList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    blocksetList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    blocksetList_->setMinimumHeight(96);
    topLayout->addWidget(blocksetList_);

    topHalf->setLayout(topLayout);
    splitter->addWidget(topHalf);

    //Construct bottom half
    QWidget* bottomHalf{ new QWidget() };
    QVBoxLayout* bottomLayout{ new QVBoxLayout() };
    bottomLayout->setMargin(0);

    QHBoxLayout* bottomSetsRow{ new QHBoxLayout() };
    previousBlocksetButton_ = new QPushButton("Previous set");
    nextBlocksetButton_     = new QPushButton("Next set");
    bottomSetsRow->addWidget(previousBlocksetButton_);
    bottomSetsRow->addWidget(nextBlocksetButton_);
    bottomLayout->addLayout(bottomSetsRow);

    int blockZoom{ 68 };
    blockList_ = new QListWidget();
    blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    blockList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    blockList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    blockList_->setMovement(QListWidget::Static);
    blockList_->setFlow(QListWidget::LeftToRight);
    blockList_->setWrapping(true);
    blockList_->setResizeMode(QListWidget::Adjust);
    blockList_->setSpacing(5);
    blockList_->setGridSize(QSize(blockZoom, blockZoom));
    blockList_->setViewMode(QListWidget::IconMode);
    blockList_->setUniformItemSizes(true);
    blockList_->setWordWrap(true);
    blockList_->setSelectionRectVisible(true);
    blockList_->setSelectionMode(QListWidget::ExtendedSelection);
    bottomLayout->addWidget(blockList_);

    QHBoxLayout* bottomBlocksRow{ new QHBoxLayout() };
    addBlockButton_    = new QPushButton("Add");
    editBlockButton_   = new QPushButton("Edit");
    removeBlockButton_ = new QPushButton("Remove");
    bottomBlocksRow->addWidget(addBlockButton_);
    bottomBlocksRow->addWidget(editBlockButton_);
    bottomBlocksRow->addWidget(removeBlockButton_);
    bottomLayout->addLayout(bottomBlocksRow);

    QVBoxLayout* slidersColumn{ new QVBoxLayout() };
    for (bool zoom: {true, false}) {

        QHBoxLayout* sliderRow{ new QHBoxLayout() };
        for (int i{0}; i < 3; ++i) {
            if (i == 1) {

                QSlider* slider{ new QSlider() };
                slider->setOrientation(Qt::Horizontal);
                slider->setTickPosition(QSlider::TicksBothSides);

                if (zoom) { zoomSlider_ = slider;

                    slider->setMinimum(32);
                    slider->setMaximum(128);
                    slider->setValue(blockZoom);
                    slider->setPageStep(12);
                    slider->setTickInterval(12);

                } else { rotationSlider_ = slider;

                    slider->setMinimum(0);
                    slider->setMaximum(360);
                    slider->setValue(135);
                    slider->setPageStep(10);
                    slider->setTickInterval(45);
                }

                sliderRow->addWidget( slider );

            } else {
                QLabel* iconLabel{ new QLabel("") };
                iconLabel->setFixedSize(QSize(16, 16));
                iconLabel->setScaledContents(true);
                iconLabel->setEnabled(false);

                switch ((i / 2) + (!zoom * 2)) {
                case 0: iconLabel->setPixmap(QPixmap(":/Minus"));
                    break;
                case 1: iconLabel->setPixmap(QPixmap(":/Plus"));
                    break;
                case 2: iconLabel->setPixmap(QPixmap(":/Redo"));
                    break;
                case 3: iconLabel->setPixmap(QPixmap(":/Undo"));
                    break;
                default:
                    break;
                }
                sliderRow->addWidget(iconLabel);
            }
        }
        slidersColumn->addLayout(sliderRow);
    }

    bottomLayout->addLayout(slidersColumn);
    bottomHalf->setLayout(bottomLayout);
    splitter->addWidget(bottomHalf);

    mainLayout->addWidget(splitter);
    setLayout(mainLayout);

    connect(addBlocksetButton_, SIGNAL(clicked(bool)), MainWindow::mainWindow_, SLOT(newBlockset()));
    connect(deleteBlocksetButton_, SIGNAL(clicked(bool)), this, SLOT(deleteBlockset()));
    connect(blocksetList_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(handleBlocksetListCurrentItemChanged()));
    connect(blocksetList_, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(handleBlocksetListItemChanged(QListWidgetItem*)));

    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    connect(nextBlocksetButton_, SIGNAL(clicked(bool)), editMaster, SLOT(NextBlockset()));
    connect(previousBlocksetButton_, SIGNAL(clicked(bool)), editMaster, SLOT(PreviousBlockset()));

    connect(blockList_, SIGNAL(currentRowChanged(int)), this, SLOT(handleBlockListCurrentRowChanged(int)));
    connect(blockList_, SIGNAL(itemSelectionChanged()), this, SLOT(handleBlockListItemSelectionChanged()));
    connect(addBlockButton_, SIGNAL(clicked(bool)), this, SLOT(editBlock()));
    connect(editBlockButton_, SIGNAL(clicked(bool)), this, SLOT(editBlock()));
    connect(removeBlockButton_, SIGNAL(clicked(bool)), this, SLOT(removeBlock()));
    connect(zoomSlider_, SIGNAL(valueChanged(int)), this, SLOT(handleZoomSliderValueChanged(int)));
    connect(rotationSlider_, SIGNAL(valueChanged(int)), this, SLOT(updateBlockPreviews()));


    handleZoomSliderValueChanged(-1);
}

void BlocksPanel::deleteBlockset()
{
    QListWidgetItem* item{ blocksetList_->currentItem() };
    Blockset* bs{ qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(PointerRole))) };

    if (bs) {

        QMessageBox* confirm{ new QMessageBox(this) };
        confirm->setWindowTitle("Delete file");
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);
        confirm->setText("Are you sure you want to delete this blockset?");
        confirm->setInformativeText(bs->name_.CString());
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;

        if (item->toolTip().contains('/'))
            GetSubsystem<FileSystem>()->Delete(item->toolTip().toStdString().data());

        GetSubsystem<EditMaster>()->Delete(bs);
    }
}
void BlocksPanel::handleCurrentBlocksetChanged(Blockset* currentBlockset)
{
    blockList_->clear();

    if (!currentBlockset) {

        blocksetList_->setCurrentRow(-1);
        deleteBlocksetButton_->setEnabled(false);

    } else {

        for (int i{0}; i < blocksetList_->count(); ++i) {

            QListWidgetItem* item{ blocksetList_->item(i) };

            if (currentBlockset == qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(PointerRole)))){

                blocksetList_->setCurrentItem(item);
                break;
            }
        }

        if (currentBlockset->blocks_.Size()) {

            for (Block* b: *currentBlockset) {

                BlockListItem* blockItem{ new BlockListItem(context_, b) };
                blockItem->setSizeHint(blockList_->gridSize());

                blockList_->addItem(blockItem);
            }
        }

        updateBlockList();
        GetSubsystem<MasterControl>()->OnTimeout();
        deleteBlocksetButton_->setEnabled(true);
    }
}
void BlocksPanel::handleBlockListCurrentRowChanged(int currentRow)
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    if (currentRow > -1) {

        Block* block{ qobject_cast<Block*>(qvariant_cast<QObject*>(blockList_->item(currentRow)->data(PointerRole))) };
        editMaster->SetCurrentBlock(block);

    } else {

        editMaster->SetCurrentBlock(nullptr);
    }

//    updateBlockList();
}
void BlocksPanel::updateBlockList()
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    Block* currentBlock{ editMaster->GetCurrentBlock() };
    QListWidget* list{ blockList_ };

    addBlockButton_->setEnabled(editMaster->GetCurrentBlockset());

    if (currentBlock == nullptr) {

        if (list->currentRow() != -1)
            list->setCurrentRow(-1);

        editBlockButton_->setEnabled(false);
        removeBlockButton_->setEnabled(false);

        return;
    }

    for (int b{}; b < list->count(); ++b) {

        Block* block{ qobject_cast<Block*>(qvariant_cast<QObject*>(list->item(b)->data(PointerRole))) };

        if (block == currentBlock) {

            if (list->currentRow() != b)
                list->setCurrentRow(b);

            editBlockButton_->setEnabled(true);
            removeBlockButton_->setEnabled(true);

            return;
        }

    }
}
void BlocksPanel::handleBlockListItemSelectionChanged()
{
    if (blockList_->selectedItems().isEmpty())
        GetSubsystem<EditMaster>()->SetCurrentBlock(nullptr);
}
void BlocksPanel::editBlock()
{
    Vector<Block*> blocks{};

    if (!addBlockButton_->hasFocus()) {

        for (QListWidgetItem* item: blockList_->selectedItems()) {

            Block* block{ qobject_cast<Block*>(qvariant_cast<QObject*>(item->data(PointerRole))) };
            blocks.Push(block);
        }
    }

    BlockDialog* blockDialog{ new BlockDialog(context_, blocks, this) };
    delete blockDialog;
}
void BlocksPanel::removeBlock()
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    Block* block{ editMaster->GetCurrentBlock() };
    int row{ blockList_->currentRow() };

    if (!(QApplication::keyboardModifiers() & Qt::ShiftModifier)) {

        QMessageBox* confirm{ new QMessageBox(this) };
        confirm->setWindowTitle("Remove block");
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);
        confirm->setText("Are you sure you want to delete this block?");
        confirm->setInformativeText(block->GetAttribute("name").GetString().CString());
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;
    }

    Blockset* currentSet{ editMaster->GetCurrentBlockset() };
    currentSet->RemoveBlock(block);
    handleCurrentBlocksetChanged(currentSet);

    if (row < blockList_->count())
        blockList_->setCurrentRow(row);
    else
        blockList_->setCurrentRow(blockList_->count() - 1);
}
void BlocksPanel::updateBlocksetList()
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    Blockset* currentSet{ editMaster->GetCurrentBlockset() };
    const Vector<Blockset*>& blocksets{ editMaster->GetBlocksets() };
    QListWidgetItem* currentItem{ nullptr };
    Project* project{ GetSubsystem<MasterControl>()->CurrentProject() };

    blocksetList_->clear();

    for (Blockset* bs: blocksets) {

        QString name{ bs->name_.CString() };
        QListWidgetItem* listItem{ new QListWidgetItem(blocksetList_) };
        String path{ project->Location() +
                     project->GetFolder("Resources") +
                     project->GetFolder("Blocksets") +
                     name.toStdString().data() };

        listItem->setIcon(QIcon(":/Blockset"));
        listItem->setText(bs->DisplayName());
        listItem->setData(StringRole, name);
        listItem->setData(PointerRole, QVariant(QMetaType::QObjectStar, &bs));
        listItem->setFlags(listItem->flags() | Qt::ItemIsEditable);

        if (fs->FileExists(path))
            listItem->setToolTip(path.CString());
        else
            listItem->setToolTip("Unsaved blockset");

        if (currentSet == bs)
            currentItem = listItem;
    }

    if (currentItem)
        blocksetList_->setCurrentItem(currentItem);
}
void BlocksPanel::handleBlocksetListItemChanged(QListWidgetItem* item)
{
    QString newText{ item->text() };

    if (newText.right(1) == "*")
        newText.chop(1);

    if (newText == item->data(StringRole).toString()) //No need for validation
        return;

    auto restoreText = [=](){ item->setText(item->data(StringRole).toString()); };

    if (newText.contains('.')) {

        QStringList splitResults{ newText.split('.', QString::SkipEmptyParts) };

        if (!splitResults.isEmpty()) {

            newText = splitResults.first();

        } else {

            restoreText();
            return;
        }
    }

    newText.replace('/', "");
    newText.replace('*', "");

    if (newText.isEmpty()) {

        restoreText();
        return;
    }

    newText.append(".ebs");

    if (item->data(StringRole).toString() != newText) {

        QListWidget* listWidget{ item->listWidget() };

        for (int i{0}; i < listWidget->count(); ++i) {

            QListWidgetItem* otherItem{ listWidget->item(i) };
            QString otherItemText{ otherItem->data(StringRole).toString() };

            if (item != otherItem && otherItemText == newText) {

                restoreText();
                return;
            }
        }

        item->setData(StringRole, newText);

        Blockset* bs{ qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(PointerRole))) };
        if (bs) {

            bs->name_ = newText.toStdString().data();
            newText = bs->DisplayName();
        }
    }

    if (item->text() != newText)
        item->setText(newText);
}
void BlocksPanel::handleBlocksetListCurrentItemChanged()
{
    QListWidgetItem* item{ blocksetList_->currentItem() };
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    if (!item) {

        editMaster->SetCurrentBlockset(nullptr);

    } else {

        editMaster->SetCurrentBlockset(qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(PointerRole))));
    }
}
void BlocksPanel::handleZoomSliderValueChanged(int value)
{
    if (value = -1)
        value = zoomSlider_->value();

    QSize gridSize{ QSize(value + value / 8, value + 16) };
    QSize iconSize{ QSize(value, value) };
    blockList_->setGridSize(gridSize);
    blockList_->setIconSize(iconSize);

    for (int b{}; b < blockList_->count(); ++b) {

        blockList_->item(b)->setSizeHint(gridSize);
    }
}
int BlocksPanel::rotationSliderValue()
{
    return rotationSlider_->value();
}
void BlocksPanel::updateBlockPreviews()
{
    for (int b{}; b < blockList_->count(); ++b) {

        static_cast<BlockListItem*>(blockList_->item(b))->UpdateBlockPreview();
    }
}
