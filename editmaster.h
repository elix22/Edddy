/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef EDITMASTER_H
#define EDITMASTER_H

#include <Urho3D/Urho3D.h>
#include "luckey.h"


URHO3D_EVENT(E_CURRENTBLOCKCHANGE, CurrentBlockChange){
    URHO3D_PARAM(P_BLOCK, Block*);
}
URHO3D_EVENT(E_CURRENTMAPCHANGE, CurrentMapChange){
    URHO3D_PARAM(P_MAP, BlockMap*);
}
URHO3D_EVENT(E_CURRENTTOOLCHANGE, CurrentToolChange){
    URHO3D_PARAM(P_TOOL, Tool*);
}

class EdddyCursor;
class Block;
class Blockset;
class BlockInstance;
class Tool;

struct BlockChange{
    BlockInstance* instance_;
    Pair<Block*, Block*> blockChange_;
    Pair<Quaternion, Quaternion> blockRotation_;
};
typedef Vector<BlockChange> UndoStep;

class EditMaster : public QObject, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(EditMaster, Object);
public:
    EditMaster(Context* context);
    EdddyCursor* GetCursor() const { return cursor_; }
    BlockMap* NewMap(const IntVector3& mapSize, const Vector3& blockSize, String name = "");
    BlockMap* LoadMap(String fileName);
    void SaveMap(BlockMap *blockMap);

    Blockset* NewBlockset(String name);
    Blockset* LoadBlockset(String fileName);
    Blockset* GetCurrentBlockset() const { return currentBlockset_; }

    void SetActiveBlockMap(BlockMap* map);
    void SetActiveBlockMap(int index);
    BlockMap* GetActiveBlockMap() const { return activeBlockMap_; }
    BlockMap* GetBlockMap(int index);
    const Vector<BlockMap*>& GetBlockMaps() { return blockMaps_; }

    unsigned ActiveLayer() const { return activeLayer_; }
    void SetActiveLayer(unsigned layer) { activeLayer_ = layer; emit activeLayerChanged(activeLayer_); }

    void SetCurrentBlockset(Blockset* blockSet);
    const Vector<Blockset*>& GetBlocksets() const { return blocksets_; }

    void NextBlock();
    void PreviousBlock();
    void SetCurrentBlock(unsigned index, Blockset* blockSet);
    void SetCurrentBlock(unsigned index);
    void SetCurrentBlock(Block* block);

    Block* GetBlock(unsigned index);
    Block* GetCurrentBlock();
    void PutBlock(IntVector3 coords, Quaternion rotation, Block* block);
    void PutBlock(IntVector3 coords);
    void PutBlock();
    void PickBlock();
    void ClearBlock(IntVector3 coords);
    void ClearBlock();

    void SetTool(Tool* tool);
    Tool* GetTool() const { return currentTool_; }
    StringHash GetLastToolType() const { return lastUsedTool_; }

    void ApplyTool(bool shiftDown, bool ctrlDown, bool altDown);
    void Undo();
    void Redo();

    void OpenMap(BlockMap* map);
    
    void Delete(BlockMap* blockMap);
    void Delete(Blockset* blockset);
signals:
    void activeLayerChanged(unsigned layer);
public slots:
    void NextBlockset();
    void PreviousBlockset();
private:
    EdddyCursor* cursor_;
    Vector<BlockMap*> blockMaps_;
    BlockMap* activeBlockMap_;
    unsigned activeLayer_;

    Vector<Blockset*> blocksets_;
    int currentBlockIndex_;
    Block* currentBlock_;
    int currentBlocksetIndex_;
    Blockset* currentBlockset_;
    Tool* currentTool_;

    Vector<UndoStep> history_;
    unsigned historyIndex_;
    StringHash lastUsedTool_;

    void CreateTools();
};

#endif // EDITMASTER_H
