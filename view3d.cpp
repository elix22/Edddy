/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "view3d.h"
#include "viewsplitter.h"

#include "mainwindow.h"
#include <QDebug>
#include <QWheelEvent>
#include <QPainter>
#include <QComboBox>
#include <QGraphicsEffect>
#include <QLayout>
#include <assert.h>
#include "edddycursor.h"
#include "edddycam.h"
#include "blockmap.h"
#include "editmaster.h"
#include "inputmaster.h"
#include "mastercontrol.h"

Vector<View3D*> View3D::views_{};
View3D* View3D::active_{};

View3D::View3D(Context* context) : QWidget(), Object(context),
    mapBox_{},
    previousMousePos_{width() / 2, height() / 2},
    renderTexture_{},
    image_{},
    cameraNode_{},
    edddyCam_{},
    blockMap_{}
{
    setMinimumSize(80, 45);
    setFocusPolicy(Qt::StrongFocus);
    setMouseTracking(true);

    mapBox_ = new QComboBox(this);
    mapBox_->move(1, 2);
    mapBox_->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    mapBox_->setVisible(false);

    QGraphicsOpacityEffect* transparent{ new QGraphicsOpacityEffect(mapBox_) };
    transparent->setOpacity(0.8);
    mapBox_->setGraphicsEffect(transparent);

    connect(mapBox_, SIGNAL(currentIndexChanged(int)), this, SLOT(SetMap(int)));

    SubscribeToEvent(E_BEGINFRAME, URHO3D_HANDLER(View3D, CreateCamera));
    SubscribeToEvent(E_CURSORSTEP, URHO3D_HANDLER(View3D, HandleCursorStep));
    SubscribeToEvent(E_CURRENTBLOCKCHANGE, URHO3D_HANDLER(View3D, HandleCursorStep));

    views_.Push(this);
    if (!active_)
        active_ = this;

    pixmap_ = QPixmap(width(), height());
    pixmap_.fill(Qt::transparent);
}
View3D::~View3D()
{
    views_.Remove(this);
}
void View3D::UpdateMapBox()
{
    mapBox_->blockSignals(true);
    mapBox_->clear();
    int i{0};
    int index{0};

    for (BlockMap* bm : GetSubsystem<EditMaster>()->GetBlockMaps()) {

        mapBox_->addItem(QIcon(":/Map"), bm->DisplayName(), QVariant(QMetaType::QObjectStar, &bm));

        if (bm == blockMap_)
            index = i;

        ++i;
    }

    if (mapBox_->count()) {

        mapBox_->blockSignals(false);
        mapBox_->updateGeometry();
        mapBox_->setCurrentIndex(index);
        BlockMap* bm{ qobject_cast<BlockMap*>(qvariant_cast<QObject*>(mapBox_->currentData(Qt::UserRole))) };

        if (blockMap_ != bm)
            SetMap(bm);

        if (mapBox_->isVisible() == false)
            mapBox_->setVisible(true);

    } else {

        mapBox_->setVisible(false);
        SetMap(nullptr);
    }
}
void View3D::SetMap(int index)
{
    SetMap(GetSubsystem<EditMaster>()->GetBlockMap(index));

    Activate();
}
void View3D::SetMap(BlockMap* blockMap)
{
    if (blockMap_ == blockMap)
        return;

    blockMap_ = blockMap;

    if (blockMap_ == nullptr)
        return;

    Scene* mapScene{ blockMap->GetScene() };

    if (mapScene == nullptr || edddyCam_->GetScene() == mapScene)
        return;

    mapScene->AddChild(cameraNode_);
    edddyCam_->HandleMapChange(blockMap);

    UpdateViewport();

    MainWindow::sidePanel()->blocksPanel()->updateBlockPreviews();

    if (active_ == this) {

        GetSubsystem<EditMaster>()->SetActiveBlockMap(blockMap);
    }

    Activate();
    UpdateMapBox();
}

void View3D::CreateCamera(StringHash, VariantMap&)
{
    CreateCamera();
}
void View3D::CreateCamera()
{
    cameraNode_ = new Node(context_);
    edddyCam_ = cameraNode_->CreateComponent<EdddyCam>();
    edddyCam_->SetView(this);

    CreateRenderTexture();

    UnsubscribeFromEvent(E_BEGINFRAME);
}
void View3D::CreateRenderTexture()
{
    if (renderTexture_)
        renderTexture_->GetRenderSurface()->Release();

    renderTexture_ = new Texture2D(context_);
    renderTexture_->SetSize(width(), height(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->GetRenderSurface()->SetUpdateMode(SURFACE_MANUALUPDATE);

    UpdateViewport();
}
void View3D::UpdateViewport()
{
    if (!blockMap_)
        return;

    Camera* camera{ edddyCam_->GetCamera() };
    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetViewport(0)) {
        renderSurface->GetViewport(0)->SetScene(blockMap_->GetScene());

    } else {

        SharedPtr<Viewport> viewport{ new Viewport(context_, blockMap_->GetScene(), camera )};
        renderSurface->SetViewport(0, viewport);
    }

    UpdateView();
}
void View3D::HandleCursorStep(StringHash, VariantMap&)
{
    if (EdddyCursor::cursor_->GetBlockMap() == blockMap_)
        UpdateView();
}
void View3D::UpdateView(StringHash, VariantMap&)
{
    UpdateView();
}
void View3D::UpdateView()
{
    if (!renderTexture_)
        return;

    renderTexture_->GetRenderSurface()->QueueUpdate();
    GetSubsystem<MasterControl>()->RequestUpdate();

    SubscribeToEvent(E_ENDRENDERING, URHO3D_HANDLER(View3D, PaintView));
}


void View3D::resizeEvent(QResizeEvent *event)
{
    MasterControl* masterControl{ GetSubsystem<MasterControl>() };
    if (!masterControl)
        return;

    CreateRenderTexture();
}
void View3D::paintEvent(QPaintEvent *event)
{
    if (blockMap_ == nullptr)
        return;

    PaintView();
    PaintBorder();
}
void View3D::PaintView(const StringHash, VariantMap&)
{
    repaint();
}
void View3D::PaintView()
{
    UpdatePixmap();

    QPainter p(this);

    int drawWidth{ static_cast<int>(ceil(pixmap_.width() * (static_cast<float>(height()) / pixmap_.height()))) };
    int dX{ width() - drawWidth };

   if (!blockMap_) {
   } else {

       if (dX > 0) {

           p.fillRect(rect(), blockMap_->GetBackgroundQColor());
       }

       p.drawPixmap(QRect(dX / 2, 0, drawWidth, height()), pixmap_);
   }
}
void View3D::UpdatePixmap()
{
    if (!blockMap_)
        return;

    Image* image{ renderTexture_->GetImage() };
    if (!image || renderTexture_->GetRenderSurface()->IsUpdateQueued())
        return;

    if (image_ != image) {

        image_ = renderTexture_->GetImage();
        pixmap_ = ImageToPixmap(image_);

        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}
void View3D::PaintBorder()
{
    if (views_.Size() == 1 || this != active_)
        return;

    QPainter p(this);

    p.setCompositionMode(QPainter::CompositionMode_Difference);
    p.setRenderHint(QPainter::Antialiasing);
    QPen pen{ QColor(32, 160, 192, 192) };
    pen.setWidth(7);
    p.setPen(pen);
    p.drawRoundedRect(-1, -1, width() + 2, height() + 2, 6.66, 6.66);

    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    pen.setWidth(6);
    p.setPen(pen);
    p.drawRoundedRect(-1, -1, width() + 2, height() + 2, 6.66, 6.66);
}
void View3D::Activate()
{
    if (active_ == this)
        return;

    View3D* lastActive{ active_ };

    active_ = this;
    GetSubsystem<EditMaster>()->SetActiveBlockMap(blockMap_);

    repaint();

    //Remove border from previously active view
    if (lastActive)
        lastActive->repaint();
}
void View3D::Split()
{
    if (views_.Size() >= 5)
        return;

    View3D* newView{ new View3D(context_) };

    QWidget* parent{ parentWidget() };

    ViewSplitter* splitter{ new ViewSplitter() };
    splitter->setParent(parentWidget());

    ViewSplitter* parentSplitter = qobject_cast<ViewSplitter*>(parent);
    QList<int> sizes{};

    if (parentSplitter) {

        sizes = parentSplitter->sizes();
        parentSplitter->insertWidget(parentSplitter->indexOf(this), splitter);

    } else {

        parent->layout()->addWidget(splitter);
    }

    splitter->addWidget(newView);
    splitter->addWidget(this);

    if (parentSplitter)
        parentSplitter->setSizes(sizes);
    splitter->show();
    this->show();

    newView->CreateCamera();
    newView->UpdateMapBox();
    newView->SetMap(blockMap_);
}

void View3D::mouseMoveEvent(QMouseEvent *event)
{
    setFocus();

    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    Qt::KeyboardModifiers modifiers{ event->modifiers() };

    Vector2 posNormalized{ static_cast<float>(event->pos().x()) / (width() - 1),
                           static_cast<float>(event->pos().y()) / (height() - 1)};
    QPoint dPos{ QCursor::pos() - previousMousePos_ };

    Vector2 dVec{ dPos.x() * 0.00125f, dPos.y() * 0.001666f };

    cursor->SetBlockMap(blockMap_);
    if (!blockMap_)
        return;

    if (edddyCam_)
        cursor->SetMouseRay(edddyCam_->GetCamera()->GetScreenRay(posNormalized.x_, posNormalized.y_));
    else
        return;

    if (QApplication::mouseButtons() & Qt::MiddleButton) {

        Activate();

        if (modifiers & Qt::ShiftModifier && modifiers & Qt::ControlModifier) {
            //Change field of view
            edddyCam_->Move(Vector3::ONE * dVec.y_, MT_FOV);

        } else if (modifiers & Qt::ShiftModifier) {
            //Pan camera
            edddyCam_->Move(Vector3(dVec.x_, dVec.y_, 0.0f), MT_PAN);

        } else if (modifiers & Qt::ControlModifier) {
            //Zoom camera
            edddyCam_->Move(Vector3::FORWARD * -dVec.y_, MT_PAN);

        } else {
            //Rotate camera
            edddyCam_->Move(Vector3(dVec.x_, dVec.y_, 0.0f), MT_ROTATE);

        }

        //Wrap cursor
        if (event->pos().x() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(width() - 2, 0));

        else if (event->pos().x() >= width())
            QCursor::setPos(QCursor::pos() - QPoint(width() - 2, 0));

        else if (event->pos().y() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(0, height() - 2));

        else if (event->pos().y() >= height())
            QCursor::setPos(QCursor::pos() - QPoint(0, height() - 2));

    } else {

        cursor->HandleMouseMove();
    }

    if (QApplication::mouseButtons() & Qt::RightButton) {

        Activate();

        editMaster->PickBlock();

    } else if (QApplication::mouseButtons() & Qt::LeftButton) {

        Activate();

        editMaster->ApplyTool(modifiers & Qt::ShiftModifier,
                              modifiers & Qt::ControlModifier,
                              modifiers & Qt::AltModifier);

    }

    previousMousePos_ = QCursor::pos();
}
void View3D::mousePressEvent(QMouseEvent *event)
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    if (event->buttons() & Qt::RightButton) {

        Activate();

        editMaster->PickBlock();

    } else if (event->buttons() & Qt::LeftButton) {

        Activate();

        Qt::KeyboardModifiers modifiers{ event->modifiers() };

        editMaster->ApplyTool(modifiers & Qt::ShiftModifier,
                              modifiers & Qt::ControlModifier,
                              modifiers & Qt::AltModifier);

    }
}
void View3D::wheelEvent(QWheelEvent *event)
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    QWheelEvent *wheelEvent = static_cast<QWheelEvent*>(event);
    int wheel{ wheelEvent->delta() };
    Qt::KeyboardModifiers modifiers{ wheelEvent->modifiers() };

    if ( wheel != 0) {

        EdddyCursor* cursor{ EdddyCursor::cursor_ };

        if (modifiers & Qt::ControlModifier) {

            if (wheel > 0)
                cursor->Rotate(true);
            if (wheel < 0)
                cursor->Rotate(false);

        } else if (modifiers & Qt::AltModifier) {

            if (wheel > 0)
                editMaster->NextBlock();
            if (wheel < 0)
                editMaster->PreviousBlock();

        } else {

            if (wheel > 0)
                cursor->Step(IntVector3::FORWARD);

            if (wheel < 0)
                cursor->Step(IntVector3::BACK);

        }
    }
}

QPixmap View3D::ImageToPixmap(Image* image)
{
    QImage qImage{ image->GetData(),
                   image->GetWidth(),
                   image->GetHeight(),
                   QImage::Format_RGBA8888};

    return QPixmap::fromImage(qImage);
}
