/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockset.h"

#include "mainwindow.h"
#include "project.h"

Blockset::Blockset(Context* context) : QObject(), Serializable(context),
    name_{},
    savedName_{},
    blocksChanged_{false}
{
}

Block* Blockset::GetBlockById(unsigned id) {

    for (Block* b : blocks_) {

        if (b->Id() == id)
            return b;

    }

    return nullptr;
}

bool Blockset::Modified() const
{
    return blocksChanged_ || name_ != savedName_;
}
bool Blockset::LoadXML(const XMLElement &source)
{
    XMLElement blockXML{ source.GetChild("block") };
    while (blockXML) {

        HashMap<unsigned, String> materials{};

        if (!blockXML.GetAttribute("material").Empty()) {

            materials[SINGLE] = blockXML.GetAttribute("material");

        } else {

            XMLElement materialXML{ blockXML.GetChild("material")};

            while (materialXML) {

                materials[materialXML.GetUInt("index")] = materialXML.GetAttribute("name");
                materialXML = materialXML.GetNext("material");
            }

        }

        AddBlock(blockXML.GetAttribute("name"),
                 blockXML.GetAttribute("model"),
                 materials,
                 blockXML.GetUInt("id"));

        blockXML = blockXML.GetNext("block");
    }
    return true;
}
bool Blockset::Save()
{
    if (!Modified())
        return true;

    XMLFile* blockSetXML{ new XMLFile(context_) };
    XMLElement rootElem{ blockSetXML->CreateRoot("blockset") };

    for (Block* b : blocks_) {

        b->SaveXML(rootElem);
    }

    Project* p{ GetSubsystem<MasterControl>()->CurrentProject() };
    String fileName{ p->Location() + p->GetFolder("Resources") + p->GetFolder("Blocksets") + name_ };

    File file{ context_, fileName, FILE_WRITE };
    if (blockSetXML->Save(file)) {

        if (name_ != savedName_) {

            fileName = p->Location() + p->GetFolder("Resources") + p->GetFolder("Blocksets") + savedName_;
            GetSubsystem<FileSystem>()->Delete(fileName);
            savedName_ = name_;
        }
        blocksChanged_ = false;

    } else {

        return false;
    }

    file.Close();

    return true;
}

QString Blockset::DisplayName() const
{
    QString displayName{ name_.CString() };

    if (Modified())
        displayName.append('*');

    return displayName;
}

void Blockset::AddBlock(String blockName, String modelName, Materials materials, unsigned id)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    Model* model{ cache->GetResource<Model>(modelName) };
    bool modified{ Modified() };

    if (model) {

        Block* block{ new Block(context_) };

        if (id == NOID) {
            id = 0;

            while (GetBlockById(id)) {

                ++id;
            }
            blocksChanged_ = true;
        }

        block->SetId(id);
        block->SetName(blockName);
        block->SetModel(model);
        block->SetMaterials(materials);

        blocks_.Push(block);

    } else {

        //Block loading failed while loading the blockset
        if (id != NOID)
            blocksChanged_ = true;
    }


    if (modified != Modified())
        MainWindow::mainWindow_->handleFileModifiedStateChange();
}

void Blockset::RemoveBlock(Block* block)
{
    if (!block || !blocks_.Contains(block))
        return;

    bool modified{ Modified() };

    blocks_.Remove(block);
    blocksChanged_ = true;

    if (modified != Modified()) {

        MainWindow::mainWindow_->handleFileModifiedStateChange();
        MainWindow::sidePanel()->blocksPanel()->updateBlocksetList();
    }
}
