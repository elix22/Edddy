/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QImage>
#include <QMessageBox>
#include <QColorDialog>
#include <QResizeEvent>
#include <QSpacerItem>
#include <QTabBar>
#include <QPainter>
#include <QDesktopWidget>
#include <QSettings>

#include "blocklistitem.h"
#include "blockmap.h"
#include "blockset.h"
#include "edddycam.h"
#include "edddycursor.h"
#include "editmaster.h"
#include "history.h"
#include "inputmaster.h"
#include "tool.h"
#include "view3d.h"
#include "viewsplitter.h"
#include "newmapdialog.h"
#include "newblocksetdialog.h"
#include "blockdialog.h"
#include "project.h"

MainWindow*   MainWindow::mainWindow_{   nullptr };
SidePanel*   MainWindow::sidePanel_{   nullptr };

MainWindow::MainWindow(Context* context) :
    QMainWindow{0, 0},
    Object{context},
    ui{new Ui::MainWindow},
    layerBoxes_{}
{
    //Influences how Urho3D writes floats to files
    std::locale::global(std::locale::classic());

    mainWindow_ = this;
    ui->setupUi(this);

    ui->actionRedo->setShortcuts({ ui->actionRedo->shortcut(), QKeySequence::fromString("Ctrl+Y") });
    //Add shortcuts for inverse axis-locking
    for (QAction* a: { ui->actionLockX,
                       ui->actionLockY,
                       ui->actionLockZ })
    {
        a->setShortcuts({ a->shortcut(), QKeySequence::fromString("Shift+" + a->shortcut().toString()) });
    }

    QSize cornerSize{ (QApplication::desktop()->geometry().size() - geometry().size()) / 2 };
    setGeometry(QRect(QPoint(cornerSize.width(), cornerSize.height()), geometry().size()));
//    delete ui->sidePanel;

    sidePanel_ = new SidePanel(context_);
    addDockWidget(Qt::LeftDockWidgetArea, sidePanel_);
//    ui->splitter->insertWidget(0, sidePanelWidget);

    connect(ui->openProjectButton, SIGNAL(clicked(bool)), ui->actionOpenProject, SLOT(trigger()));
    connect(ui->newProjectButton,  SIGNAL(clicked(bool)), ui->actionNewProject,  SLOT(trigger()));

//    ui->splitter->setProperty("storedWidth", 0);

    QAction* hidePanel{ new QAction(this) };
    hidePanel->setShortcut(QKeySequence("Ctrl+`"));
    connect(hidePanel, SIGNAL(triggered()), this, SLOT(toggleSidePanel()));
    addAction(hidePanel);
}

void MainWindow::LoadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("mainwindow/geometry").toByteArray());
    restoreState(settings.value("mainwindow/state").toByteArray());

    CreateRecentProjectButtons();
}
void MainWindow::RestoreGeometry()
{
    QSettings settings{};

    QRect lastGeometry{ settings.value("mainwindow/geometry").toRect() };
    if (!lastGeometry.isEmpty()) {

        QRect desktopGeometry{ QApplication::desktop()->geometry() };
        if (desktopGeometry.contains(lastGeometry)) {

            setGeometry(lastGeometry);

        } else if (desktopGeometry.height() >= lastGeometry.height()
                && desktopGeometry.width()  >= lastGeometry.width()) {

            QSize cornerRect{ (desktopGeometry.size() - lastGeometry.size()) / 2 };
            setGeometry(QRect(QPoint(cornerRect.width(), cornerRect.height()), lastGeometry.size()));
        }
    }

    Qt::WindowState mainWindowState{ settings.value("mainwindow/state").value<Qt::WindowState>() };
    
    if (mainWindowState != Qt::WindowState::WindowMinimized)
        setWindowState(mainWindowState);
}
void MainWindow::CreateRecentProjectButtons()
{
    MasterControl* mc{ GetSubsystem<MasterControl>() };
    QSettings settings{};

    for (QString recent: settings.value("recentprojects").toStringList()) {

        if (!GetSubsystem<FileSystem>()->FileExists(String(recent.toStdString().data())))
            continue;

        QPushButton* recentButton{ new QPushButton{} };

        recentButton->setObjectName(recent);
        recentButton->setToolTip(recent);

        recent.chop(4);
        recent = recent.split('/', QString::SkipEmptyParts).last();
        recentButton->setText(recent);
        QFont font{ recentButton->font() };
        font.setPixelSize(16);
        font.setBold(true);
        recentButton->setFont(font);
        recentButton->setFlat(true);
        recentButton->setMaximumHeight(24);
        recentButton->setCursor(Qt::PointingHandCursor);
        ui->noProjectLayout->insertWidget(ui->noProjectLayout->count() - 1, recentButton);

        connect(recentButton, SIGNAL(clicked(bool)), mc, SLOT(OpenRecentProject()));
    }
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (!GetSubsystem<MasterControl>()->Quit())
        event->ignore();
}
MainWindow::~MainWindow()
{
    QSettings settings{};

    settings.setValue("mainwindow/geometry", saveGeometry());
    settings.setValue("mainwindow/state", saveState());

    delete ui;
}
void MainWindow::CreateLayerButtons()
{
    EditMaster* em{ GetSubsystem<EditMaster>() };
    QWidget* expander{ new QWidget() };
    expander->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->mainToolBar->addWidget(expander);

    for (int i{0}; i < 5; ++i) {

        QCheckBox* layerBox{ new QCheckBox(QString::number(i + 1)) };
        layerBox->setObjectName(layerBox->text());
        layerBox->setLayoutDirection(Qt::RightToLeft);
        ui->mainToolBar->addWidget(layerBox);
        layerBoxes_.push_back(layerBox);

        connect(layerBox,   SIGNAL(clicked(bool)),                  this, SLOT(layerBoxClicked()));
        connect(em,         SIGNAL(activeLayerChanged(unsigned)),   this, SLOT(activeLayerChanged(unsigned)));
    }
}
void MainWindow::layerBoxClicked()
{
    GetSubsystem<EditMaster>()->SetActiveLayer(std::stoi(sender()->objectName().toStdString()));
}
void MainWindow::activeLayerChanged(unsigned layer)
{
    for (QCheckBox* layerBox: layerBoxes_) {

        layerBox->setChecked(std::stoi(layerBox->objectName().toStdString()) == layer);
    }
}
void MainWindow::Initialize()
{
    MainWindow::mainWindow_->CreateLayerButtons();
    activeLayerChanged(GetSubsystem<EditMaster>()->ActiveLayer());

    LoadSettings();
    show();

    ui->actionBrush->trigger();
    setAxisLock(std::bitset<3>( 1|(1<<2) ));
    handleOpenProject();

//    QList<int> splitterSizes{ ui->splitter->sizes() };
//    int totalSize{ splitterSizes[0] + splitterSizes[1] };
//    splitterSizes[0] = 0;
//    splitterSizes[1] = totalSize;
//    ui->splitter->setSizes(splitterSizes);
}
void MainWindow::toggleSidePanel()
{
//    QList<int> splitterSizes{ ui->splitter->sizes() };
//    int totalSize{ splitterSizes[0] + splitterSizes[1] };


//    if (!splitterSizes[0]) {

//        int panelWidth{ ui->splitter->property("storedWidth").toInt() };

//        if (panelWidth == 0) {
//            panelWidth = std::max(300, (totalSize / 3) - width() / 10);

//        }

//        splitterSizes[0] = panelWidth;
//        splitterSizes[1] = totalSize - splitterSizes[0];

//    } else {

//        ui->splitter->setProperty("storedWidth", splitterSizes.at(0));
//        splitterSizes[0] = 0;
//        splitterSizes[1] = totalSize;
//    }

//    ui->splitter->setSizes(splitterSizes);
}
void MainWindow::updateWindowTitle()
{
    Project* currentProject{ GetSubsystem<MasterControl>()->CurrentProject() };

    if (currentProject)
        setWindowTitle(currentProject->TrimmedName() + (currentProject->Modified() ? "*" : "") + " - Edddy");
    else
        setWindowTitle("Edddy");
}

void MainWindow::handleOpenProject()
{
    UpdateMapActions();

    Project* currentProject{ GetSubsystem<MasterControl>()->CurrentProject() };

    ui->actionSaveProject->setEnabled(currentProject);
    ui->actionNewMap->setEnabled(currentProject);
    sidePanel()->tabWidget()->setEnabled(currentProject);


    if (currentProject) {

        currentProject->OnResourcesFolderSet();

        sidePanel()->tabWidget()->setCurrentIndex(0);
        updateWindowTitle();

    }

    sidePanel()->projectPanel()->updatePanel();
}
void MainWindow::UpdateMapActions()
{
    bool activeMap{ GetSubsystem<EditMaster>()->GetActiveBlockMap() != nullptr };

    ui->actionSaveMap->setEnabled(activeMap);
    ui->actionUndo->setEnabled(activeMap);
    ui->actionRedo->setEnabled(activeMap);
    ui->actionBrush->setEnabled(activeMap);
    ui->actionFill->setEnabled(activeMap);
    ui->actionSplitView->setEnabled(activeMap);
    ui->actionLockX->setEnabled(activeMap);
    ui->actionLockY->setEnabled(activeMap);
    ui->actionLockZ->setEnabled(activeMap);

    sidePanel()->mapPanel()->setEnabled(activeMap);
}

void MainWindow::removeNoProjectLayout()
{
    if (/*ui->centralWidget->itemAt(0) == */ui->noProjectLayout) {

        while (QLayoutItem* item = ui->noProjectLayout->takeAt(0)) {

            item->widget()->deleteLater();
        }
        ui->centralWidget->layout()->removeItem(ui->noProjectLayout);

        ui->centralWidget->layout()->addWidget(new View3D(context_));
        GetSubsystem<MasterControl>()->RequestUpdate();
    }

//    QList<int> splitterSizes{ ui->splitter->sizes() };
//    if (!splitterSizes[0]) {

//        int totalSize{ splitterSizes[0] + splitterSizes[1] };
//        splitterSizes[0] = std::max(300, (totalSize / 3) - mainWindow_->width() / 10);
//        splitterSizes[1] = totalSize - splitterSizes[0];
//        ui->splitter->setSizes(splitterSizes);
//    }
}

void MainWindow::on_actionNewProject_triggered()
{
    GetSubsystem<MasterControl>()->NewProject(QFileDialog::getSaveFileName(
                        this, tr("New Project"), "/home", tr("EDY Files (*.edy)")).toLatin1().data());
}
void MainWindow::on_actionOpenProject_triggered()
{
    MasterControl* mc { GetSubsystem<MasterControl>() };
    Project* project{ mc->CurrentProject() };

    mc->OpenProject(QFileDialog::getOpenFileName(
                        this, tr("Open Project"), (project ? project->GetAttribute("Location").ToString().CString()
                                                           : "/home"),
                        tr("EDY Files (*.edy)")).toLatin1().data());
}
void MainWindow::on_actionSaveMap_triggered()
{
    GetSubsystem<EditMaster>()->GetActiveBlockMap()->Save();

    for (View3D* v: View3D::views_) {
        v->UpdateMapBox();
    }

    sidePanel()->projectPanel()->updateBrowser();
}

void MainWindow::on_actionUndo_triggered()
{
    GetSubsystem<EditMaster>()->Undo();
}
void MainWindow::on_actionRedo_triggered()
{
    GetSubsystem<EditMaster>()->Redo();
}

void MainWindow::on_actionBrush_triggered()
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    editMaster->SetTool(Tool::GetTool(StringHash("Brush")));

    ui->actionFill->setChecked(false);
    ui->actionBrush->setChecked(true);
}
void MainWindow::on_actionFill_triggered()
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    editMaster->SetTool(Tool::GetTool(StringHash("Fill")));

    ui->actionBrush->setChecked(false);
    ui->actionFill->setChecked(true);
}

void MainWindow::on_actionAboutEdddy_triggered()
{
    QString aboutText{ QString("<p><b>") +  "Edddy" + QString(" ")
                + qApp->applicationVersion() + QString(" </b></p>")
                + tr("<p>Copyleft 🄯 2019 <a href=\"https://luckeyproductions.nl\">LucKey Productions</a></b>"
                     "<p>You may use and redistribute this software under the terms "
                     "of the<br><a href=\"http://www.gnu.org/licenses/gpl.html\">"
                     "GNU General Public License Version 3</a>.</p>") };

    QMessageBox::about(this, tr("About %1").arg("Edddy"), aboutText);
}
void MainWindow::setAxisLock(std::bitset<3> lock)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    cursor->SetAxisLock(QApplication::keyboardModifiers() & Qt::ShiftModifier ? lock.flip()
                                                                              : lock);
    ui->actionLockX->setChecked(lock[0]);
    ui->actionLockY->setChecked(lock[1]);
    ui->actionLockZ->setChecked(lock[2]);
}

void MainWindow::on_actionLockX_triggered()
{
    std::bitset<3> lock{}; lock[0] = true;
    setAxisLock(lock);
}
void MainWindow::on_actionLockY_triggered()
{
    std::bitset<3> lock{}; lock[1] = true;
    setAxisLock(lock);
}
void MainWindow::on_actionLockZ_triggered()
{
    std::bitset<3> lock{}; lock[2] = true;
    setAxisLock(lock);
}

void MainWindow::handleFileModifiedStateChange()
{
    updateWindowTitle();
    sidePanel()->projectPanel()->updateBrowser();

    for (View3D* v: View3D::views_) {
        v->UpdateMapBox();
    }
}

void MainWindow::on_actionSplitView_triggered()
{
    if (View3D::views_.Size())
        View3D::Active()->Split();
}

void MainWindow::on_actionSaveProject_triggered()
{
    GetSubsystem<MasterControl>()->SaveCurrentProject();
}

void MainWindow::on_actionNewMap_triggered()
{
    NewMapDialog* dialog{ new NewMapDialog(context_, this) };
    delete dialog;
}
void MainWindow::newBlockset()
{
    NewBlocksetDialog* dialog{ new NewBlocksetDialog(context_) };
    delete dialog;

    sidePanel()->projectPanel()->updateBrowser();
}
