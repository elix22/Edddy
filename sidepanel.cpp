/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>

#include "sidepanel.h"

SidePanel::SidePanel(Context* context, QWidget *parent) : QDockWidget(parent), Object(context)
{
    setFeatures(DockWidgetMovable|DockWidgetFloatable);
    setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);

    tabWidget_ = new QTabWidget();
    projectPanel_ = new ProjectPanel(context_);
    mapPanel_ = new MapPanel(context_);
    blocksPanel_ = new BlocksPanel(context_);

    tabWidget_->addTab(projectPanel_,  QIcon(":/Edddy"),    "Project");
    tabWidget_->addTab(mapPanel_,      QIcon(":/Map"),      "Map");
    tabWidget_->addTab(blocksPanel_,   QIcon(":/Blockset"), "Blocks");
    tabWidget_->addTab(new QWidget(),  QIcon(":/Cluster"),  "Clusters");
    tabWidget_->addTab(new QWidget(),  QIcon(":/Brush"),    "Tool");

    setWidget(tabWidget_);
    tabWidget_->setTabPosition(QTabWidget::West);

    connect(this, SIGNAL(dockLocationChanged(Qt::DockWidgetArea)), this, SLOT(onDockLocationChanged(Qt::DockWidgetArea)));
}

void SidePanel::onDockLocationChanged(Qt::DockWidgetArea area)
{
    switch (area) {
    case Qt::LeftDockWidgetArea:
        tabWidget_->setTabPosition(QTabWidget::West);
        break;
    case Qt::RightDockWidgetArea:
        tabWidget_->setTabPosition(QTabWidget::East);
        break;
    default:
        break;
    }
}
