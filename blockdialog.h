/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKDIALOG_H
#define BLOCKDIALOG_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QSplitter>
#include <QHBoxLayout>
#include <QLabel>

#include "luckey.h"
#include "blocklistitem.h"
#include "block.h"

class BlockDialog : public QDialog, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(BlockDialog, Object);
public:
    explicit BlockDialog(Context* context, Vector<Block*> blocks, QWidget* parent = nullptr);
protected:
    void showEvent(QShowEvent*e) override;
private slots:
    void accept();
    void selectedModelChanged();
    void selectedMaterialChanged();
    void updateMaterialButtons();
private:
    Vector<Block*> blocksIn_;
    Vector<Block*> previewBlocks_;
    Vector<BlockListItem*> previewItems_;

    QSplitter* splitter_;
    QListWidget* previewList_;
    QListWidget* modelList_;
    QListWidget* materialList_;
    QLabel* materialLabel_;
    QWidget* materialButtons_;
    QDialogButtonBox* buttonBox_;

    void updateOkButton();
    void PopulateModelList();
    void PopulateMaterialList();
};

#endif // BLOCKDIALOG_H
