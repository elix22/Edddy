/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "view3d.h"
#include "inputmaster.h"
#include "edddycursor.h"
#include "blockinstance.h"
#include "blockmap.h"
#include "blockset.h"
#include "edddyevents.h"
#include "history.h"
#include "project.h"

#include "tool.h"
#include "brush.h"
#include "fill.h"

#include "editmaster.h"

EditMaster::EditMaster(Context* context) : QObject(), Object(context),
    blockMaps_{},
    activeBlockMap_{},
    activeLayer_{ 1 },
    currentBlockIndex_{ -1 },
    currentBlock_{},
    currentBlocksetIndex_{ -1 },
    currentBlockset_{},
    currentTool_{}
{
    CreateTools();

    Node* cursorNode{ new Node(context_) };
    cursor_ = cursorNode->CreateComponent<EdddyCursor>();

}
void EditMaster::CreateTools()
{
    new Brush(context_);
    new Fill(context_);
}

BlockMap* EditMaster::NewMap(const IntVector3& mapSize, const Vector3& blockSize, String name)
{
    BlockMap* newBlockMap{ (new Scene(context_))->CreateComponent<BlockMap>() };

    newBlockMap->SetName(name);
    newBlockMap->SetMapSize(mapSize);
    newBlockMap->SetBlockSize(blockSize);
    newBlockMap->Initialize();

    blockMaps_.Push(newBlockMap);

    return newBlockMap;
}

BlockMap* EditMaster::LoadMap(String fileName)
{
    BlockMap* map{ nullptr };
    String name{ fileName.Split('/').Back() };

    for (BlockMap* bm: blockMaps_) {

        if (name == bm->GetSavedName()) {

            map = bm;
            break;
        }
    }

    if (!map) {

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        XMLFile* mapXML{ cache->GetResource<XMLFile>(fileName) };

        if (!mapXML)
            return nullptr;


        XMLElement rootElem{ mapXML->GetRoot("blockmap") };

        if (rootElem) {

            BlockMap* blockMap{ (new Scene(context_))->CreateComponent<BlockMap>() };
            if (blockMap->LoadXML(rootElem)) {

                String name{ fileName.Split('/').Back() };

                blockMap->SetFileName(fileName);
                blockMap->SetName(name);
                blockMap->SetSavedName(name);
                blockMap->GetHistory()->HandleLoad();
                blockMaps_.Push(blockMap);

                map = blockMap;
            }
        }
    }
    if (map) {

        OpenMap(map);
        MainWindow::sidePanel()->projectPanel()->updateBrowser();
    }

    return map;
}
void EditMaster::OpenMap(BlockMap* map)
{
    for (View3D* view: View3D::views_) {

        view->UpdateMapBox();
    }

    View3D::Active()->SetMap(map);
    MainWindow::mainWindow_->UpdateMapActions();
}

Blockset* EditMaster::NewBlockset(String name)
{
    Blockset* newBlockset{ new Blockset(context_) };
    newBlockset->name_ = name;

    blocksets_.Push(newBlockset);
    MainWindow::sidePanel()->blocksPanel()->updateBlocksetList();

    return newBlockset;
}
Blockset* EditMaster::LoadBlockset(String fileName)
{
    String name{ fileName.Split('/').Back() };

    for (Blockset* bs: blocksets_) {

        if (name == bs->savedName_) {

            return bs;
        }
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    XMLFile* blockSetXML{ cache->GetResource<XMLFile>(fileName) };
    if (!blockSetXML)
        return nullptr;

    XMLElement rootElem{ blockSetXML->GetRoot("blockset") };

    Blockset* newBlockset{ new Blockset(context_) };

    newBlockset->LoadXML(rootElem);
    newBlockset->savedName_ = newBlockset->name_ = name;
    blocksets_.Push(newBlockset);

    return newBlockset;
}

void EditMaster::SetActiveBlockMap(BlockMap* map)
{
    if (!map || activeBlockMap_ == map)
        return;

    activeBlockMap_ = map;

    MainWindow::sidePanel()->mapPanel()->updateInfo();
}
BlockMap* EditMaster::GetBlockMap(int index)
{
    if (!blockMaps_.Empty()
     && index >= 0
     && index < blockMaps_.Size())

        return blockMaps_.At(index);
    else
        return nullptr;
}
void EditMaster::SetActiveBlockMap(int index)
{
    if (!blockMaps_.Empty()
     && index >= 0
     && index < blockMaps_.Size())

        SetActiveBlockMap(blockMaps_.At(index));
}
void EditMaster::SetCurrentBlockset(Blockset* blockSet)
{
    if (currentBlockset_ == blockSet)
        return;

    currentBlockset_ = blockSet;

    if (currentBlockset_) {

        currentBlocksetIndex_ = blocksets_.IndexOf(currentBlockset_);

    } else {

        currentBlocksetIndex_ = -1;
    }

    MainWindow::sidePanel()->blocksPanel()->handleCurrentBlocksetChanged(currentBlockset_);
}
void EditMaster::NextBlockset()
{
    if (!blocksets_.Size())
        return;

    if (++currentBlocksetIndex_ >= blocksets_.Size()) {

        currentBlocksetIndex_ = 0;
    }

    SetCurrentBlockset(blocksets_.At(currentBlocksetIndex_));
}
void EditMaster::PreviousBlockset()
{
    if (!blocksets_.Size())
        return;

    if (currentBlocksetIndex_-- <= 0) {

        currentBlocksetIndex_ = blocksets_.Size() - 1;
    }

    SetCurrentBlockset(blocksets_.At(currentBlocksetIndex_));
}
void EditMaster::SetCurrentBlock(unsigned index, Blockset* blockSet)
{
    if (!blockSet || index < 0) {

        SetCurrentBlock(nullptr);
        return;
    }

    SetCurrentBlockset(blockSet);

    if (index >= currentBlockset_->blocks_.Size()) {

        SetCurrentBlock(nullptr);
        return;
    }

    currentBlockIndex_ = index;
    currentBlock_ = currentBlockset_->blocks_[currentBlockIndex_];

    VariantMap eventData{};
    eventData[CurrentBlockChange::P_BLOCK] = currentBlock_;

    SendEvent(E_CURRENTBLOCKCHANGE, eventData);

    MainWindow::sidePanel()->blocksPanel()->updateBlockList();

    GetSubsystem<MasterControl>()->RequestUpdate();
}

void EditMaster::SetCurrentBlock(unsigned index)
{
    SetCurrentBlock(index, currentBlockset_);
}
void EditMaster::SetCurrentBlock(Block* block)
{
    if (currentBlock_ == block)
        return;

    if (block == nullptr) {

        currentBlock_ = nullptr;

        VariantMap eventData{};
        eventData[CurrentBlockChange::P_BLOCK] = currentBlock_;

        SendEvent(E_CURRENTBLOCKCHANGE, eventData);

        MainWindow::sidePanel()->blocksPanel()->updateBlockList();

        GetSubsystem<MasterControl>()->RequestUpdate();

    } else for (Blockset* blockSet : blocksets_) {

        if (blockSet->blocks_.Contains(block)) {

            for (unsigned b{0}; b < blockSet->blocks_.Size(); ++b) {

                if (blockSet->blocks_[b] == block) {

                    SetCurrentBlockset(blockSet);
                    SetCurrentBlock(b);

                    return;
                }
            }
        }
    }
}

void EditMaster::NextBlock()
{
    if (!currentBlock_) {

        SetCurrentBlock(currentBlockIndex_);
        return;
    }

    if (++currentBlockIndex_ >= currentBlockset_->blocks_.Size())
        currentBlockIndex_ = 0;

    SetCurrentBlock(currentBlockIndex_);
}
void EditMaster::PreviousBlock()
{
    if (!currentBlock_) {

        SetCurrentBlock(currentBlockIndex_);
        return;
    }

    if (currentBlockIndex_-- == 0)
        currentBlockIndex_ = currentBlockset_->blocks_.Size() - 1;

    SetCurrentBlock(currentBlockIndex_);
}

Block* EditMaster::GetBlock(unsigned index)
{
    if (index >= currentBlockset_->blocks_.Size()) {

        return nullptr;

    } else {

        return currentBlockset_->blocks_[index];

    }
}
Block* EditMaster::GetCurrentBlock()
{
    return currentBlock_;
}

void EditMaster::PickBlock()
{
    if (!GetActiveBlockMap())
        return;

    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    BlockInstance* blockInstance{ GetActiveBlockMap()->GetBlockInstance(cursor->GetCoords(), activeLayer_) };

    if (blockInstance && blockInstance->GetBlock()) {

        Log::Write(LOG_INFO, blockInstance->GetBlock()->modelName());

        cursor->SetRotation(blockInstance->GetRotation());
        SetCurrentBlock(blockInstance->GetBlock());

    } else {

        Log::Write(LOG_INFO, "Empty space");

        SetCurrentBlock(nullptr);
    }

    //Clear last used tool
    lastUsedTool_ = StringHash{};
}

void EditMaster::PutBlock(IntVector3 coords, Quaternion rotation, Block* block)
{
    if (!activeBlockMap_)
        return;

    Change change{};
    change.position_.first_ = change.position_.second_ = Vector3(coords);

    change.instance_ = GetActiveBlockMap()->GetBlockInstance(coords, activeLayer_);
    change.block_.first_ = change.instance_->GetBlock();
    change.rotation_.first_ = change.instance_->GetRotation();

    GetActiveBlockMap()->SetGridBlock(coords, rotation, block, activeLayer_);

    change.block_.second_ = change.instance_->GetBlock();
    change.rotation_.second_ = change.instance_->GetRotation();

    if (change.Any()) {

        activeBlockMap_->GetHistory()->AddChange(change);
        activeBlockMap_->UpdateViews();
    }
}

void EditMaster::PutBlock(IntVector3 coords)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    PutBlock(coords, cursor->GetRotation(), currentBlock_);
}
void EditMaster::PutBlock()
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    if (cursor->IsHidden())
        return;

    PutBlock(cursor->GetCoords());
}
void EditMaster::ClearBlock(IntVector3 coords)
{
    PutBlock(coords, Quaternion::IDENTITY, nullptr);
}
void EditMaster::ClearBlock()
{
    ClearBlock(EdddyCursor::cursor_->GetCoords());
}

void EditMaster::SetTool(Tool* tool)
{
    if (!tool || tool == currentTool_)
        return;

    lastUsedTool_ = StringHash{};
    currentTool_ = tool;

    currentTool_->UpdatePreview(false, false, false);

    VariantMap eventData{};
    eventData[CurrentToolChange::P_TOOL] = tool;
    SendEvent(E_CURRENTTOOLCHANGE, eventData);

    EdddyCursor::cursor_->GetBlockMap()->UpdateViews();
}
void EditMaster::ApplyTool(bool shiftDown, bool ctrlDown, bool altDown)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    if (cursor->IsHidden() || !currentTool_ || !activeBlockMap_)
        return;

    currentTool_->Apply(shiftDown, ctrlDown, altDown);
    currentTool_->UpdatePreview(shiftDown, ctrlDown, altDown);

    lastUsedTool_ = currentTool_->GetType();
}
void EditMaster::Undo()
{
    if (activeBlockMap_) {

        if (activeBlockMap_->GetHistory()->Undo())
            activeBlockMap_->UpdateViews();
    }
}
void EditMaster::Redo()
{
    if (activeBlockMap_) {

        if (activeBlockMap_->GetHistory()->Redo())
            activeBlockMap_->UpdateViews();
    }
}

void EditMaster::Delete(BlockMap* blockMap)
{
    for (BlockMap* bm: blockMaps_) {

        if (bm == blockMap) {

            blockMaps_.Remove(blockMap);

            MainWindow::sidePanel()->projectPanel()->updateBrowser();

            if (blockMap->Modified())
                MainWindow::mainWindow_->updateWindowTitle();

            for (View3D* view: View3D::views_) {

                if (view->GetBlockMap() == bm) {

                    ///Switch to other map or close view
                }

                view->UpdateMapBox();
            }
        }
    }
}
void EditMaster::Delete(Blockset* blockset)
{
    ///Check maps for using this blockset first

    for (Blockset* bs: blocksets_) {

        if (bs == blockset) {

            bool modified{bs->Modified()};

            blocksets_.Remove(blockset);
            delete blockset;

            MainWindow::sidePanel()->projectPanel()->updateBrowser();
            MainWindow::sidePanel()->blocksPanel()->updateBlocksetList();

            if (modified)
                MainWindow::mainWindow_->updateWindowTitle();

        }
    }
}
