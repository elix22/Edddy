TARGET = edddy

QT += core gui widgets

LIBS += ../Edddy/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    ../Edddy/Urho3D/include \
    ../Edddy/Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

SOURCES += \
    luckey.cpp \
    mastercontrol.cpp \
    inputmaster.cpp \
    edddycam.cpp \
    castmaster.cpp \
    effectmaster.cpp \
    gridblock.cpp \
    blockmap.cpp \
    blockinstance.cpp \
    freeblock.cpp \
    block.cpp \
    edddycursor.cpp \
    editmaster.cpp \
    blockset.cpp \
    history.cpp \
    tool.cpp \
    brush.cpp \
    fill.cpp \
    empfile.cpp \
    mainwindow.cpp \
    main.cpp \
    view3d.cpp \
    blocklistitem.cpp \
    viewsplitter.cpp \
    project.cpp \
    newmapdialog.cpp \
    blockdialog.cpp \
    modifieddialog.cpp \
    newblocksetdialog.cpp \
    projectpanel.cpp \
    blockspanel.cpp \
    mappanel.cpp \
    sidepanel.cpp

HEADERS += \
    luckey.h \
    mastercontrol.h \
    inputmaster.h \
    edddycam.h \
    castmaster.h \
    effectmaster.h \
    edddyevents.h \
    gridblock.h \
    blockmap.h \
    blockinstance.h \
    freeblock.h \
    block.h \
    edddycursor.h \
    editmaster.h \
    blockset.h \
    history.h \
    tool.h \
    brush.h \
    fill.h \
    empfile.h \
    blockmapdefs.h \
    mainwindow.h \
    view3d.h \
    blocklistitem.h \
    viewsplitter.h \
    project.h \
    newmapdialog.h \
    blockdialog.h \
    modifieddialog.h \
    newblocksetdialog.h \
    projectpanel.h \
    blockspanel.h \
    mappanel.h \
    sidepanel.h


FORMS += \
    mainwindow.ui

RESOURCES += \
    resources.qrc

INSTALLS += \
    #binary
    #mime types
    #icons
