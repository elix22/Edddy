/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKLISTITEM_H
#define BLOCKLISTITEM_H

#include <QListWidgetItem>
#include "luckey.h"

class Block;

class BlockListItem : public QListWidgetItem, public Object
{
    URHO3D_OBJECT(BlockListItem, Object)

public:
    BlockListItem(Context* context, Block* block);

    void UpdateBlockPreview();
private:
    Block* block_;
    void UpdateIcon(const StringHash, VariantMap&);
    QIcon ImageToIcon(Image* image);
};

#endif // BLOCKLISTITEM_H
