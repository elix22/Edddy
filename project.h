/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PROJECT_H
#define PROJECT_H

#include <QMessageBox>

#include "luckey.h"

class ModifiedBox : public QMessageBox, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(ModifiedBox, Object)

public:
    ModifiedBox(Context* context, QWidget* parent = nullptr);
};

class Project : public QObject, public Serializable
{
    Q_OBJECT
    URHO3D_OBJECT(Project, Serializable)

public:
    explicit Project(Context* context);

    static void RegisterObject(Context* context);
    bool Load(const String fileName);
    bool Save();

    String Path();
    String GetFolder(const String& folderName);
    QString TrimmedName();

    String Location();
    void OnResourcesFolderSet();
    bool Close();
    bool Modified();
private:
    String name_;
    String location_;
    String previousResourceFolder_;
    String resourceFolder_;
    String modelsFolder_;
    String materialsFolder_;
    String mapsFolder_;
    String blocksetsFolder_;

    bool unsavedChanges_;
    void OnFolderSet();
};

#endif // PROJECT_H
