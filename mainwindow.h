/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QCheckBox>

#include "luckey.h"

#include "sidepanel.h"

namespace Ui {
class MainWindow;
}

class Blockset;
class Block;

class MainWindow : public QMainWindow, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(MainWindow, Object)

public:
    static MainWindow*   mainWindow_;

    explicit MainWindow(Context* context);
    ~MainWindow();
    void CreateLayerButtons();

    void setAxisLock(std::bitset<3> lock);
    void removeNoProjectLayout();

    void LoadSettings();
    void Initialize();
    void handleOpenProject();

    void UpdateMapActions();
    void updateWindowTitle();

    void handleFileModifiedStateChange();

    static SidePanel* sidePanel() { return sidePanel_; }
protected slots:
    void closeEvent(QCloseEvent* event) override;
private slots:
    void on_actionQuit_triggered();

    void on_actionNewProject_triggered();
    void on_actionOpenProject_triggered();
    void on_actionSaveProject_triggered();

    void on_actionNewMap_triggered();
    void on_actionSaveMap_triggered();

    void on_actionUndo_triggered();
    void on_actionRedo_triggered();

    void on_actionBrush_triggered();
    void on_actionFill_triggered();

    void on_actionSplitView_triggered();
    void on_actionLockX_triggered();
    void on_actionLockY_triggered();
    void on_actionLockZ_triggered();

    void on_actionAboutEdddy_triggered();

    void newBlockset();
    void toggleSidePanel();
    void layerBoxClicked();
    void activeLayerChanged(unsigned layer);
private:
    static SidePanel*   sidePanel_;
    Ui::MainWindow*     ui;
    QVector<QCheckBox*> layerBoxes_;

    void RestoreGeometry();
    void CreateRecentProjectButtons();

};

#endif // MAINWINDOW_H
