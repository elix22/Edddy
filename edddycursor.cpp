/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "edddycam.h"
#include "inputmaster.h"
#include "effectmaster.h"
#include "castmaster.h"
#include "editmaster.h"
#include "blockmap.h"

#include "edddycursor.h"

EdddyCursor* EdddyCursor::cursor_{};

EdddyCursor::EdddyCursor(Context* context) : LogicComponent(context),
    coords_{},
    mouseRay_{},
    rotation_{},
    blockModelGroups_{},
    previewNodes_{},
    axisLock_{}
//    previousAxisLock_{}
{
    cursor_ = this;



//    previousAxisLock_ = axisLock_;
//    previousAxisLock_.flip();
}

void EdddyCursor::OnNodeSet(Node *node)
{ if (!node) return;

    blockNode_ = node_->CreateChild("PREVIEW");

    RigidBody* rigidBody_{ node_->CreateComponent<RigidBody>() };
    rigidBody_->SetKinematic(true);

    for (int c{0}; c < 3; ++c){

        CollisionShape* hitPlane{ node_->CreateComponent<CollisionShape>() };
        hitPlane->SetBox(Vector3(c == 0 ? 0.0f : 1000.0f,
                                 c == 1 ? 0.0f : 1000.0f,
                                 c == 2 ? 0.0f : 1000.0f));
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    boxNode_ = node_->CreateChild("BOX");
    boxModel_ = boxNode_->CreateComponent<StaticModel>();
    boxModel_->SetModel(cache->GetTempResource<Model>("Models/Cursor.mdl"));
    boxModel_->SetMaterial(cache->GetTempResource<Material>("Materials/GlowWire.xml"));

    for (bool wire : {true, false}) {

        StaticModelGroup* blockModelGroup{ blockNode_->CreateComponent<StaticModelGroup>() };

        if (wire) {
            blockModelGroups_.first_ = blockModelGroup;
        } else {
            blockModelGroups_.second_ = blockModelGroup;
        }
    }
    AddInstanceNode(blockNode_);

    SubscribeToEvent(E_CURRENTBLOCKCHANGE, URHO3D_HANDLER(EdddyCursor, UpdateModel));
}
void EdddyCursor::SetBlockMap(BlockMap* blockMap)
{
    if (blockMap == blockMap_)
        return;

    BlockMap* previousBlockMap{ blockMap_ };

    if (blockMap) {

        blockMap_ = blockMap;
        UpdateSize();
        blockMap->GetScene()->AddChild(node_);
        MoveTo(blockMap->GetCenter());

    } else {

        blockMap_ = nullptr;
    }
    if (previousBlockMap)
        previousBlockMap->UpdateViews();
}
void EdddyCursor::UpdateModel(StringHash eventType, VariantMap& eventData)
{ (void)eventType;

    Block* currentBlock{ static_cast<Block*>(eventData[CurrentBlockChange::P_BLOCK].GetPtr()) };

    if (currentBlock) {

        Model* model{ currentBlock->model() };
        if (model) {

            ResourceCache* cache{ GetSubsystem<ResourceCache>() };

            blockModelGroups_.first_->SetModel(model);
            blockModelGroups_.first_->SetMaterial(cache->GetTempResource<Material>("Materials/GlowWire.xml"));
            blockModelGroups_.second_->SetModel(model);
            blockModelGroups_.second_->SetMaterial(cache->GetTempResource<Material>("Materials/TransparentGlow.xml"));

            if (boxNode_->IsEnabled())
                boxNode_->SetEnabled(false);
        }
    } else {

        blockModelGroups_.first_->SetModel(nullptr);
        blockModelGroups_.second_->SetModel(nullptr);

        if (!boxNode_->IsEnabled())
            boxNode_->SetEnabled(true);
    }
}

void EdddyCursor::Hide()
{
    hidden_ = true;
    boxNode_->SetEnabled(false);
    blockNode_->SetEnabled(false);

    SendEvent(E_CURSORSTEP);
}
void EdddyCursor::Show()
{
    hidden_ = false;
    blockNode_->SetEnabled(true);
    if (blockModelGroups_.first_->GetModel()) {
        boxNode_->SetEnabled(false);
    } else {
        boxNode_->SetEnabled(true);
    }
}
void EdddyCursor::ToggleVisibility()
{
    hidden_ ? Show()
            : Hide();
}

void EdddyCursor::AddInstanceNode(Node* node, Quaternion rotation)
{ if (!node) return;

    if (node == blockNode_) {

        blockModelGroups_.first_->AddInstanceNode(node);
        blockModelGroups_.second_->AddInstanceNode(node);

    } else {

        Node* previewNode{ node->GetChild("PREVIEW") };

        if (!previewNode)
            previewNode = node->CreateChild("PREVIEW");

        previewNode->SetWorldRotation(rotation);
//        blockModelGroups_.first_->AddInstanceNode(previewNode);
        blockModelGroups_.second_->AddInstanceNode(previewNode);

        previewNodes_.Push(previewNode);
    }
}
void EdddyCursor::RemoveInstanceNode(Node* node)
{ if (!node) return;

    if (node == blockNode_) {

        blockModelGroups_.first_->RemoveInstanceNode(node);
        blockModelGroups_.second_->RemoveInstanceNode(node);

    } else {

        Node* previewNode{ node->GetChild("PREVIEW") };

        if (!previewNode)
            return;

//        blockModelGroups_.first_->RemoveInstanceNode(previewNode);
        blockModelGroups_.second_->RemoveInstanceNode(previewNode);

        if (previewNodes_.Contains(previewNode)) {
            previewNodes_.Remove(previewNode);
            previewNode->Remove();
        }
    }
}
void EdddyCursor::RemoveAllInstanceNodes()
{
    if (!GetScene())
        return;

    blockModelGroups_.first_->RemoveAllInstanceNodes();
    blockModelGroups_.second_->RemoveAllInstanceNodes();
    
    AddInstanceNode(blockNode_);

    for (Node* node: previewNodes_)
        node->Remove();

    previewNodes_.Clear();
}

void EdddyCursor::UpdateSize()
{
    BlockMap* currentMap{ GetSubsystem<EditMaster>()->GetActiveBlockMap() };
    Vector3 blockSize{ Vector3::ONE };

    if (currentMap)
        blockSize = currentMap->GetBlockSize();

    boxNode_->SetScale(Vector3(blockSize.x_, blockSize.y_, blockSize.z_));
}

void EdddyCursor::SetAxisLock(std::bitset<3> lock)
{
    if (lock.all() || lock.none())
        return;

    if (lock != axisLock_){
        axisLock_ = lock;

        SendEvent(E_CURSORSTEP);
    }
}

void EdddyCursor::Step(IntVector3 step)
{
    if (step == IntVector3::ZERO)
        return;

    IntVector3 mapSize{ IntVector3::ONE };

    if (blockMap_)
        mapSize = blockMap_->GetMapSize();

    switch (axisLock_.to_ulong()) {
    case 5: step = IntVector3(step.x_, step.z_, step.y_);
        break;
    case 3: step = IntVector3(step.x_, step.y_, step.z_);
        break;
    case 6: step = IntVector3(step.z_, step.y_, step.x_);
        break;

    case 1: step = IntVector3(step.x_ + step.y_ + step.z_, 0, 0);
        break;
    case 2: step = IntVector3(0, step.x_ + step.y_ + step.z_, 0);
        break;
    case 4: step = IntVector3(0, 0, step.x_ + step.y_ + step.z_);
        break;
    default:
        break;
    }


    IntVector3 resultingCoords{ coords_ + step };
    if (resultingCoords.x_ < 0
     || resultingCoords.y_ < 0
     || resultingCoords.z_ < 0
     || resultingCoords.x_ >= mapSize.x_
     || resultingCoords.y_ >= mapSize.y_
     || resultingCoords.z_ >= mapSize.z_)
    {
        return;
    }

    SetCoords(resultingCoords);
}

void EdddyCursor::SetCoords(IntVector3 coords)
{

    if (!blockMap_ || (coords == coords_ && !hidden_))
        return;

    Vector3 blockSize{ blockMap_->GetBlockSize() };

    if (!blockMap_->Contains(coords)) {

        if (!hidden_)
            Hide();

        return;

    } else {

        if (hidden_)
            Show();
    }

    coords_ = coords;

    GetSubsystem<EffectMaster>()->TranslateTo(node_, Vector3(coords) * blockSize, 0.023f);
//    node_->SetPosition(Vector3(coords) * blockSize);
    SendEvent(E_CURSORSTEP);
}

void EdddyCursor::MoveTo(Vector3 position)
{
    //if current layer type == BLOCKS_GRID
    Vector3 blockSize{ blockMap_ ? blockMap_->GetBlockSize() : Vector3::ONE };

    IntVector3 coords{ VectorRoundToInt(position / blockSize) };

    SetCoords(IntVector3(
            axisLock_[0] ? coords.x_ : coords_.x_,
            axisLock_[1] ? coords.y_ : coords_.y_,
            axisLock_[2] ? coords.z_ : coords_.z_));
}

void EdddyCursor::Rotate(bool clockWise)
{
    Vector3 axis{ Vector3::UP };
    rotation_ = Quaternion(clockWise ? 90.0f :  -90.0f, axis) * rotation_;
    GetSubsystem<EffectMaster>()->RotateTo(node_, rotation_, 0.13f);

    SendEvent(E_CURSORSTEP);
}
void EdddyCursor::SetRotation(Quaternion rot)
{
    node_->SetRotation(rot);

    if (rotation_ != rot) {

        rotation_ = rot;
        SendEvent(E_CURSORSTEP);
    }
}
void EdddyCursor::Update(float timeStep)
{
    if (node_->GetAttributeAnimation("Rotation") || node_->GetAttributeAnimation("Position"))

        blockMap_->UpdateViews();
}

void EdddyCursor::HandleMouseMove()
{
    if (!GetScene())
        return;

    PODVector<PhysicsRaycastResult> hitResults{};

    if (GetSubsystem<CastMaster>()->PhysicsRayCast(GetScene(), hitResults, mouseRay_, M_LARGE_VALUE, M_MAX_UNSIGNED)){

        float closest{ M_INFINITY };
        PhysicsRaycastResult closestResult{};

        for (PhysicsRaycastResult result: hitResults)
            if (((((axisLock_.count() == 2) != (axisLock_[1] != axisLock_[2])) && Abs(result.normal_.x_) > 0.5f)
              || (((axisLock_.count() == 2) != (axisLock_[0] != axisLock_[2])) && Abs(result.normal_.y_) > 0.5f)
              || (((axisLock_.count() == 2) != (axisLock_[0] != axisLock_[1])) && Abs(result.normal_.z_) > 0.5f))
              && result.distance_ < closest)
            {
                closest = result.distance_;
                closestResult = result;
            }
        if (closestResult.body_)
            MoveTo(closestResult.position_);
    }
}
