/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QFileDialog>

#include "editmaster.h"
#include "project.h"
#include "blockmap.h"
#include "blockset.h"
#include "view3d.h"
#include "mainwindow.h"

#include "projectpanel.h"

ProjectPanel::ProjectPanel(Context* context, QWidget *parent) : QWidget(parent), Object(context),
    projectNameLabel_{ new QLabel() },
    resourcesEdit_{ new QLineEdit() },
    resourcesResetButton_{ new QPushButton() },
    folderEdits_{},
    resourceBrowser_{ new QTreeWidget() }
{
    QVBoxLayout* mainLayout{ new QVBoxLayout() };
    mainLayout->setContentsMargins(5, 2, 5, 5);
    mainLayout->setSpacing(4);

    QFont projectNameLabelFont{ projectNameLabel_->font() };
    projectNameLabelFont.setItalic(true);
    projectNameLabelFont.setPointSize(11);
    projectNameLabel_->setFont(projectNameLabelFont);
    projectNameLabel_->setAlignment(Qt::AlignHCenter);
    mainLayout->addWidget(projectNameLabel_);

    QFormLayout* primaryForm{ new QFormLayout() };
    primaryForm->setContentsMargins(2, 5, 0, 0);
    QHBoxLayout* resourcesRow{ new QHBoxLayout() };
    resourcesEdit_->setObjectName("Resources");
    resourcesRow->addWidget(resourcesEdit_);

    QPushButton* resourcesPickButton{  new QPushButton() };
    resourcesPickButton->setIcon(QIcon(":/Open"));
    resourcesPickButton->setToolTip("Pick resources folder");
    resourcesPickButton->setProperty("lineedit", QVariant(QMetaType::QObjectStar, &resourcesEdit_));
    resourcesRow->addWidget(resourcesPickButton);

    resourcesResetButton_->setObjectName("resetResourcesButton");
    resourcesResetButton_->setIcon(QIcon(":/Undo"));
    resourcesResetButton_->setToolTip("Revert resources folder");
    resourcesResetButton_->setProperty("lineedit", QVariant(QMetaType::QObjectStar, &resourcesEdit_));
    resourcesRow->addWidget(resourcesResetButton_);

    primaryForm->addRow("Resources folder:", resourcesRow);
    mainLayout->addLayout(primaryForm);

    connect(resourcesEdit_,        SIGNAL(editingFinished()), this, SLOT(onFolderEditFinished()));
    connect(resourcesPickButton,   SIGNAL(clicked(bool)),     this, SLOT(onFolderPickClicked()));
    connect(resourcesResetButton_, SIGNAL(clicked(bool)),     this, SLOT(onFolderResetClicked()));

    QFrame* primaryLine{ new QFrame() };
    primaryLine->setFrameShape(QFrame::HLine);
    primaryLine->setFrameShadow(QFrame::Sunken);
    mainLayout->addWidget(primaryLine);

    QFormLayout* secondaryForm{ new QFormLayout() };
    secondaryForm->setContentsMargins(5, 5, 0, 17);
    secondaryForm->setSpacing(6);
    secondaryForm->setLabelAlignment(Qt::AlignRight);

    for (QString folder: folders) {

        QHBoxLayout* folderRow{ new QHBoxLayout() };
        folderRow->setSpacing(2);

        QLineEdit* folderEdit{ new QLineEdit() };
        folderEdit->setObjectName(folder);
        folderEdits_.push_back(folderEdit);

        QPushButton* folderPickButton{  new QPushButton() };
        folderPickButton->setIcon(QIcon(":/Open"));
        folderPickButton->setToolTip("Pick " + folder.toLower() + " folder");
        folderPickButton->setProperty("lineedit", QVariant(QMetaType::QObjectStar, &folderEdit));

        QPushButton* folderResetButton{ new QPushButton() };
        folderResetButton->setObjectName("reset" + folder + "Button");
        folderResetButton->setIcon(QIcon(":/Undo"));
        folderResetButton->setToolTip("Revert " + folder.toLower() + " folder");
        folderResetButton->setProperty("lineedit", QVariant(QMetaType::QObjectStar, &folderEdit));

        folderRow->addWidget(folderEdit);
        folderRow->addWidget(folderPickButton);
        folderRow->addWidget(folderResetButton);
        secondaryForm->addRow(folder + ":", folderRow);

        connect(folderEdit, SIGNAL(editingFinished()), this, SLOT(onFolderEditFinished()));
        connect(folderPickButton, SIGNAL(clicked(bool)), this, SLOT(onFolderPickClicked()));
        connect(folderResetButton, SIGNAL(clicked(bool)), this, SLOT(onFolderResetClicked()));
    }

    mainLayout->addLayout(secondaryForm);

    resourceBrowser_->setHeaderHidden(true);
    mainLayout->addWidget(resourceBrowser_);

    setLayout(mainLayout);

    resourceBrowser_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(resourceBrowser_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showBrowserMenu(QPoint)));
    connect(resourceBrowser_, SIGNAL(itemActivated(QTreeWidgetItem*, int)), this, SLOT(resourceBrowserItemActivated(QTreeWidgetItem*)));
}
void ProjectPanel::onFolderEditFinished()
{
    MasterControl* mc{ GetSubsystem<MasterControl>() };

    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(sender()) };
    String name{ lineEdit->objectName().toStdString().data() };
    String attribute{ name  + "Folder" };
    String folder{ lineEdit->text().toStdString().data() };
    String input{ folder };

    while (folder.Find('.') != String::NPOS) {
        folder.Erase(folder.Find('.'));
    }

    if (folder.Contains('/') && folder.Length() > 1)
        folder = folder.Split('/').Front().Append('/');

    if (!folder.Contains('/'))
        folder = folder.Append('/');

    if (input != folder)
        lineEdit->setText(folder.CString());

    Project* project{ mc->CurrentProject() };

    if (project->GetAttribute(attribute).GetString() != folder) {

        project->SetAttribute(attribute, folder);
    }
}
void ProjectPanel::onFolderPickClicked()
{
    QPushButton* pickButton{ qobject_cast<QPushButton*>(sender()) };
    Project* project{ GetSubsystem<MasterControl>()->CurrentProject() };
    QString projectLocation{ project->GetAttribute("Location").ToString().CString() };
    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(qvariant_cast<QObject*>(pickButton->property("lineedit"))) };
    String name{ lineEdit->objectName().toStdString().data() };
    bool resourcesFolder{ name == "Resources" };

    QString path { QFileDialog::getExistingDirectory(this, tr("Pick folder"),
                                                     resourcesFolder ? projectLocation
                                                                     : projectLocation + project->GetAttribute("ResourcesFolder").ToString().CString()) };

    if (path.contains(projectLocation)) {

        QString folder{ path.split('/').last().append('/') };
        if (lineEdit->text() != folder) {

            project->SetAttribute(name + "Folder", folder.toStdString().data());
            lineEdit->setText(folder);
        }
    }
}
void ProjectPanel::onFolderResetClicked()
{
    MasterControl* mc{ GetSubsystem<MasterControl>() };
    QPushButton* resetButton{ qobject_cast<QPushButton*>(sender()) };
    Project* project{ mc->CurrentProject() };
    QString name{ resetButton->objectName() };
    name.chop(6);
    name = name.right(name.length() - 5);

    String attribute{ name.toStdString().data() };
    attribute.Append("Folder");

    String defaultValue{ project->GetAttributeDefault(attribute) };
    project->SetAttribute(attribute, defaultValue);

    QLineEdit* lineEdit{ qobject_cast<QLineEdit*>(qvariant_cast<QObject*>(resetButton->property("lineedit"))) };
    lineEdit->setText(defaultValue.CString());
    updateBrowser();
}

void ProjectPanel::updatePanel()
{
    Project* project{ GetSubsystem<MasterControl>()->CurrentProject() };

    if (project) {

        projectNameLabel_->setText(project->TrimmedName());
        resourcesEdit_->setText(project->GetFolder("Resources").CString());

        for (QLineEdit* folderEdit: folderEdits_) {
            for (QString folder: folders) {

                if (folderEdit->objectName() == folder)
                    folderEdit->setText(project->GetFolder(folder.toStdString().data()).CString());
            }
        }

    } else {

        projectNameLabel_->setText("Project Name");
    }

    updateBrowser();
}
void ProjectPanel::updateBrowser()
{
    Project* project{ GetSubsystem<MasterControl>()->CurrentProject() };

    if (!project) {

        resourceBrowser_->clear();
        return;
    }

    FileSystem* fs{ GetSubsystem<FileSystem>() };
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    StringVector expanded{};
    storeExpandedFolders(expanded);

    resourceBrowser_->clear();

    QTreeWidgetItem* resourcesFolderItem{ new QTreeWidgetItem(resourceBrowser_) };
    String resourcesFolder{ project->GetFolder("Resources") };
    String resourcesPath{ project->Location() + resourcesFolder };

    resourcesFolderItem->setText(0, resourcesFolder.Substring(0, resourcesFolder.Length() - 1).CString());

    if (fs->DirExists(resourcesPath)) {

        resourcesFolderItem->setIcon(0, QIcon(":/Open"));

        StringVector folderNames{};
        for (String subFolder: {"Models", "Materials", "Maps", "Blocksets"}) {

            String folderName{ project->GetFolder(subFolder) };
            if (!folderNames.Contains(folderName))
                folderNames.Push(folderName);
        }

        for (String folderName: folderNames) {

            if (folderName == "/") ///TEMP
                continue;

            QTreeWidgetItem* subFolderItem{ new QTreeWidgetItem(resourcesFolderItem) };
            String text{ RemoveTrailingSlash(folderName) };
            subFolderItem->setText(0, text.CString());
            if (expanded.Contains(text))
                subFolderItem->setExpanded(true);

            String path{ resourcesPath + folderName };

            if (fs->DirExists(path)) {

                ResourceCache* cache{ GetSubsystem<ResourceCache>() };
                subFolderItem->setIcon(0, QIcon(":/Open"));

                if (folderName == project->GetFolder("Models")) {

                    Vector<String> scanResults{};
                    fs->ScanDir(scanResults, path, "*.mdl", SCAN_FILES, false);

                    Sort(scanResults.Begin(), scanResults.End());

                    for (String modelFile: scanResults) {

                        if (cache->GetResource<Model>(path + modelFile)) {

                            QTreeWidgetItem* modelItem{ new QTreeWidgetItem(subFolderItem) };
                            modelItem->setText(0, modelFile.CString());
                            modelItem->setIcon(0, QIcon(":/Model"));
                            modelItem->setToolTip(0, (path + modelFile).CString());
                        }
                    }
                }

                if (folderName == project->GetFolder("Materials")) {

                    Vector<String> scanResults{};
                    fs->ScanDir(scanResults, path, "*.xml", SCAN_FILES, false);

                    Sort(scanResults.Begin(), scanResults.End());

                    for (String materialFile: scanResults) {

                        if (cache->GetResource<Material>(folderName + materialFile)) {

                            QTreeWidgetItem* materialItem{ new QTreeWidgetItem(subFolderItem) };
                            materialItem->setText(0, materialFile.CString());
                            materialItem->setIcon(0, QIcon(":/Material"));
                            materialItem->setToolTip(0, (path + materialFile).CString());
                        }
                    }
                }
                if (folderName == project->GetFolder("Maps")) {

                    Vector<BlockMap*> remainingMaps{ editMaster->GetBlockMaps() };
                    Vector<String> scanResults{};
                    fs->ScanDir(scanResults, path, "*.emp", SCAN_FILES, false);

                    for (BlockMap* map: remainingMaps) {

                        String mapName{ map->GetSavedName() };
                        if (mapName.Empty())
                            mapName = map->GetName();

                        if (!scanResults.Contains(mapName))
                            scanResults.Push(map->GetName());
                    }
                    Sort(scanResults.Begin(), scanResults.End());

                    for (String mapFile: scanResults) {

                        String fullPath{ path + mapFile };
                        QTreeWidgetItem* mapItem{ new QTreeWidgetItem() };
                        mapItem->setIcon(0, QIcon(":/Map"));
                        mapItem->setToolTip(0, fullPath.CString());

                        QString itemText{ mapFile.CString() };
                        BlockMap* bm{ nullptr };

                        for (BlockMap* map: remainingMaps) {

                            if ((!map->GetFileName().Empty() && map->GetFileName() == fullPath)
                                                             || map->GetName()     == mapFile)
                            {
                                bm = map;
                                remainingMaps.Remove(map);
                                break;
                            }
                        }

                        if (bm) {

                            itemText = bm->DisplayName();
                            mapItem->setData(0, PointerRole, QVariant(QMetaType::QObjectStar, &bm));
                        }
                        mapItem->setText(0, itemText);
                        subFolderItem->addChild(mapItem);
                    }

                    if (expanded.Empty())
                        subFolderItem->setExpanded(true);
                }
                if (folderName == project->GetFolder("Blocksets")) {

                    Vector<String> scanResults{};
                    Vector<String> blocksetNames{};
                    fs->ScanDir(scanResults, path, "*.ebs", SCAN_FILES, false);

                    for (String blocksetFile: scanResults)
                        editMaster->LoadBlockset(folderName + blocksetFile);

                    for (Blockset* bs: editMaster->GetBlocksets())
                        blocksetNames.Push(bs->name_);

                    Sort(blocksetNames.Begin(), blocksetNames.End());


                    for (String blocksetName: blocksetNames) {

                        Blockset* bs{ nullptr };
                        for (Blockset* set: editMaster->GetBlocksets()) {

                            if (set->name_ == blocksetName) {

                                bs = set;
                                break;
                            }
                        }

                        if (bs) {

                            QTreeWidgetItem* blocksetItem{ new QTreeWidgetItem() };
                            blocksetItem->setIcon(0, QIcon(":/Blockset"));
                            blocksetItem->setText(0, bs->DisplayName());
                            blocksetItem->setData(0, PointerRole, QVariant(QMetaType::QObjectStar, &bs));

                            if (!bs->savedName_.Empty())
                                blocksetItem->setToolTip(0, (path + blocksetName).CString());
                            else
                                blocksetItem->setToolTip(0, "Unsaved blockset");

                            subFolderItem->addChild(blocksetItem);
                        }
                    }

                    if (scanResults.Size()) {

                        MainWindow::sidePanel()->blocksPanel()->updateBlocksetList();
                    }
                }

            } else {

                subFolderItem->setIcon(0, QIcon(":/MissingFolder"));
            }
        }

    } else {

        resourcesFolderItem->setIcon(0, QIcon(":/MissingFolder"));
    }

    resourceBrowser_->addTopLevelItem(resourcesFolderItem);
    resourcesFolderItem->setExpanded(true);
}
void ProjectPanel::storeExpandedFolders(StringVector& expanded)
{
    for (int i{0}; i < resourceBrowser_->topLevelItemCount(); ++i) {

        QTreeWidgetItem* topLevelItem{ resourceBrowser_->topLevelItem(i) };
        if (!topLevelItem->isExpanded())
            break;

        for (int j{0}; j < topLevelItem->childCount(); ++j) {

            QTreeWidgetItem* childItem{ topLevelItem->child(j) };
            if (childItem->isExpanded())
                expanded.Push(childItem->text(0).toStdString().data());
        }
    }
}
void ProjectPanel::resourceBrowserItemActivated(QTreeWidgetItem* item)
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    String itemText{ item->text(0).toStdString().data() };

    if (!itemText.Contains('.'))
        return;

    if (itemText.Back() == '*')
        itemText.Resize(itemText.Length() - 1);

    Vector<String> splitVector{ itemText.Split('.') };
    if (splitVector.Size() < 2)
        return;

    String extension{ splitVector.Back() };

    if (extension == "emp") {


        String filename{ itemText };

        while (item->parent()) {

            item = item->parent();
            if (!item->parent())
                break;

            filename.Insert(0, item->text(0).append('/').toStdString().data());
        }


        BlockMap* bm{ qobject_cast<BlockMap*>(qvariant_cast<QObject*>(item->data(0, PointerRole))) };
        if (bm) {

            View3D::Active()->SetMap(bm);
            return;

        } else {

            editMaster->LoadMap(filename);
            return;
        }

    } else if (extension == "ebs") {

        Blockset* bs{ qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(0, PointerRole))) };
        if (bs) {

            editMaster->SetCurrentBlockset(bs);
            MainWindow::mainWindow_->sidePanel()->tabWidget()->setCurrentIndex(2);
        }
    }
}
void ProjectPanel::showBrowserMenu(const QPoint& pos)
{
    QPoint globalPos{ resourceBrowser_->mapToGlobal(pos) };

    QMenu browserMenu{};

    resourceBrowser_->setCurrentItem(resourceBrowser_->itemAt(pos));
    QTreeWidgetItem* selected{ resourceBrowser_->currentItem() };

    if (selected && selected->text(0).contains('.')) { ///Store item type somehow

        browserMenu.addAction(QIcon(":/Edit"),   "Edit",   this, SLOT(editBrowserItem()));
        browserMenu.addAction(QIcon(":/Delete"), "Delete", this, SLOT(deleteSelectedFile()));
        browserMenu.addSeparator();
    }
    browserMenu.addAction(QIcon(":/Material"), "New Material", this, SLOT(errorCatcher()));
    browserMenu.addAction(QIcon(":/Map"),      "New Map",      MainWindow::mainWindow_, SLOT(on_actionNewMap_triggered()));
    browserMenu.addAction(QIcon(":/Blockset"), "New Blockset", MainWindow::mainWindow_, SLOT(newBlockset()));

    QList<QAction*> actions{ browserMenu.actions() };
    for (int a{0}; a < actions.size(); ++a) {

        if (actions.at(a)->text() == "New Material")
            actions.at(a)->setEnabled(false);
    }
    browserMenu.exec(globalPos);
}
void ProjectPanel::editBrowserItem()
{
    resourceBrowserItemActivated(resourceBrowser_->currentItem());
}
void ProjectPanel::deleteSelectedFile()
{
    bool deleted{ false };
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    QTreeWidgetItem* item{ resourceBrowser_->currentItem() };
    String fileName{ item->toolTip(0).toStdString().data() };

    if (item->text(0).contains('.')) {

        QMessageBox* confirm{ new QMessageBox(this) };
        confirm->setWindowTitle("Delete file");
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);

        QString file{ item->text(0) };
        if (file.right(1) == '*')
            file.chop(1);

        confirm->setText("Are you sure you want to delete " + file + "?");
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;

        if (fileName.Contains('/') && fs->FileExists(fileName)) {

            deleted = fs->Delete(fileName);
        }

        EditMaster* editMaster{ GetSubsystem<EditMaster>() };
        Blockset* bs{ qobject_cast<Blockset*>(qvariant_cast<QObject*>(item->data(0, PointerRole))) };

        if (bs) {
            editMaster->Delete(bs);

        } else {
            BlockMap* bm{ qobject_cast<BlockMap*>(qvariant_cast<QObject*>(item->data(0, PointerRole))) };

            if (bm) {
                editMaster->Delete(bm);
            }
        }
    }
}
