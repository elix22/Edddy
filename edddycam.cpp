/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "edddycam.h"
#include "edddycursor.h"
#include "inputmaster.h"
#include "blockmap.h"
#include "editmaster.h"
#include "view3d.h"

EdddyCam::EdddyCam(Context *context):
    LogicComponent(context)
{
}

void EdddyCam::OnNodeSet(Node *node)
{ if (!node) return;

    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(4096.0f);
    camera_->SetFov(60.0f);

    node_->SetRotation(Quaternion(23.0f, 0.0f, 0.0f));
}
void EdddyCam::SetView(View3D* view)
{
    view3d_ = view;
}
void EdddyCam::HandleMapChange(BlockMap* blockMap)
{
    node_->SetPosition(-Max(Max(blockMap->GetMapWidth(),
                                blockMap->GetMapHeight()),
                                blockMap->GetMapDepth()) * node_->GetDirection() * Sqrt(blockMap->GetBlockSize().Length() * 0.42f)
                       + blockMap->GetCenter() + Vector3::DOWN * blockMap->GetMapHeight() * 0.666f);


}

void EdddyCam::ToggleOrthogaphic()
{
    camera_->SetOrthographic(!camera_->IsOrthographic());

    if (view3d_)
        view3d_->UpdateView();
}

void EdddyCam::Move(Vector3 movement, MoveType type)
{
    if (movement == Vector3::ZERO)
        return;

    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    Vector3 cursorPosition{ cursor->GetNode()->GetPosition() };
    float cursorDistance{ cursorPosition.DistanceToPlane(GetPosition(), node_->GetDirection()) };

    bool ortho{ camera_->IsOrthographic() };
    float panSpeed{ 1.5f + (ortho ? (camera_->GetOrthoSize() * 1.3f) : (cursorDistance * camera_->GetFov() * 0.028f)) };
    float rotationSpeed{ 235.0f };

    switch (type) {
    case MT_FOV: {

        if (!ortho)
            camera_->SetFov(Clamp(camera_->GetFov() + 23.0f * movement.y_, 5.0f, 120.0f));
        else
            node_->Translate(Vector3::BACK * movement.y_ * panSpeed);

    } break;
    case MT_PAN: {

        movement *= panSpeed;

        if (!ortho) {

            node_->Translate(node_->GetRight() * -movement.x_ +
                             node_->GetUp() * movement.y_ +
                             node_->GetDirection() * movement.z_, TS_WORLD);

        } else {

            camera_->SetOrthoSize(Max(1.0f, camera_->GetOrthoSize() + movement.z_ * -1.3f));
            Vector3 lockVector{ cursor->GetLockVector() };

            if (lockVector.Length() != 1.0f) { ///Needs more cases or generalisation

                if (lockVector.y_ == 0.0f)
                    movement.y_ /= Abs(node_->GetDirection().DotProduct(Vector3::UP) * M_SQRT2);
//                else
//                    movement.x_ /= Abs(node_->GetDirection().DotProduct(Vector3::FORWARD) * M_SQRT2);
            }

            node_->Translate((lockVector * node_->GetRight()).Normalized() * -movement.x_
                           + (lockVector * node_->GetUp()).Normalized()    *  movement.y_, TS_WORLD);

        }

    } break;
    default: case MT_ROTATE: {

        float pitchDelta{ movement.y_ * rotationSpeed };
        ClampPitch(pitchDelta);

        node_->RotateAround(cursorPosition,
                            Quaternion(movement.x_ * rotationSpeed, Vector3::UP) *
                            Quaternion(pitchDelta, node_->GetRight()), TS_WORLD);
    }
    }

    if (view3d_)
        view3d_->UpdateView();
}

void EdddyCam::ClampPitch(float& pitchDelta)
{
    float resultingPitch{ node_->GetRotation().PitchAngle() + pitchDelta };

    if (resultingPitch > PITCH_MAX) {

        pitchDelta -= resultingPitch - PITCH_MAX;

    } else if (resultingPitch < PITCH_MIN) {

        pitchDelta -= resultingPitch - PITCH_MIN;
    }
}
