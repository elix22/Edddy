/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SIDEPANEL_H
#define SIDEPANEL_H

#include <QDockWidget>
#include <QTabWidget>

#include "projectpanel.h"
#include "mappanel.h"
#include "blockspanel.h"

class SidePanel : public QDockWidget, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(SidePanel, Object)
public:
    explicit SidePanel(Context* context, QWidget *parent = nullptr);

    QTabWidget*   tabWidget() { return tabWidget_; }
    ProjectPanel* projectPanel() { return projectPanel_; }
    MapPanel*     mapPanel() { return mapPanel_; }
    BlocksPanel*  blocksPanel() { return blocksPanel_; }
signals:

public slots:
private slots:
    void onDockLocationChanged(Qt::DockWidgetArea area);
private:
    QTabWidget*   tabWidget_;
    ProjectPanel* projectPanel_;
    MapPanel*     mapPanel_;
    BlocksPanel*  blocksPanel_;
};

#endif // SIDEPANEL_H
