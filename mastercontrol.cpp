/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <QPainter>
#include <QDebug>
#include <QSettings>
#include <QDesktopWidget>

#include "inputmaster.h"
#include "castmaster.h"
#include "effectmaster.h"
#include "editmaster.h"
#include "project.h"

#include "view3d.h"
#include "edddycam.h"
#include "edddycursor.h"

#include "blockmap.h"
#include "block.h"
#include "gridblock.h"
#include "freeblock.h"



MasterControl::MasterControl(int & argc, char** argv, Context* context) :
    QApplication(argc, argv),
    Application(context),
    argument_{},
    mainWindow_{ new MainWindow(context_) },
    ui_{},
    renderer_{},
    defaultStyle_{},
    scene_{},
    requireUpdate_{ true },
    drawDebug_{ false }
{
    RegisterFactory<EdddyCam>();
    RegisterFactory<EdddyCursor>();
    Project::RegisterObject(context_);
    RegisterFactory<BlockMap>();
    RegisterFactory<GridBlock>();
    RegisterFactory<FreeBlock>();

    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckeyprodutions.nl");
    QCoreApplication::setApplicationName("Edddy");

    if (argc == 2) {
        const QString argument{ QString(argv[1]) };
        if (argument.contains(".edy"))
            argument_ = argument.toStdString().data();
    }
}
MasterControl::~MasterControl()
{
    delete mainWindow_;
}

void MasterControl::Setup()
{
    QWidget* fakeWidget{ new QWidget() };

    engineParameters_[EP_WINDOW_TITLE] = "Edddy";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") + "edddy.log";
    engineParameters_[EP_RESOURCE_PATHS] = "EditorResources;";
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(fakeWidget->winId());
    engineParameters_[EP_WORKER_THREADS] = true;

    delete fakeWidget;
}

void MasterControl::Start()
{
    SetRandomSeed(TIME->GetSystemTime());
    GetSubsystem<ResourceCache>()->SetAutoReloadResources(true);

    context_->RegisterSubsystem(this);
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<CastMaster>();
    RegisterSubsystem<EffectMaster>();
    RegisterSubsystem<EditMaster>();

    mainWindow_->Initialize();

    if (!argument_.Empty()) {

        OpenProject(argument_);
    }

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(OnTimeout()));
    timer.start(5);

    SubscribeToEvents();
    exec();
    Exit();
}

void MasterControl::OnTimeout()
{
    if (requireUpdate_ && !mainWindow_->isMinimized()
            && engine_ && !engine_->IsExiting()) {

        RunFrame();
    }
}
void MasterControl::RunFrame()
{
    requireUpdate_ = false;
    engine_->RunFrame();

    qDebug() << TIME->GetTimeStamp().CString() << " Ran frame";
}

void MasterControl::NewProject(QString path)
{
    if (path.isEmpty())
        return;

    if (!path.endsWith(".edy", Qt::CaseInsensitive))
        path += ".edy";

    QString projectName{ path.split('/', QString::SplitBehavior::SkipEmptyParts).last() };
    path.chop(projectName.length());

    Project* newProject_ = new Project(context_);
    newProject_->SetAttribute("Name",     String(projectName.toStdString().data()));
    newProject_->SetAttribute("Location", String(path.toStdString().data()));

    if (!project_)
        mainWindow_->removeNoProjectLayout();

    project_ = newProject_;

    mainWindow_->handleOpenProject();
}

void MasterControl::OpenProject(String path)
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    if (path.Empty() || GetExtension(path) != ".edy")
        return;


    if (path.At(0) != '/')
        path = fs->GetCurrentDir() + path;

    Log::Write(LOG_INFO, path);

    String projectName{ path.Split('/').Back() };
    path.Resize(path.Length() - projectName.Length());

    Project* newProject_{ new Project(context_) };

    if (!newProject_->Load(path + projectName))
        return;

    newProject_->SetAttribute("Location", path);
    newProject_->SetAttribute("Name",     projectName);

    if (!project_)
        mainWindow_->removeNoProjectLayout();
    else
        project_->Close();

    project_ = newProject_;
    mainWindow_->handleOpenProject();

    //Update recent files
    path += projectName;
    UpdateRecentProjects(path.CString());
}
void MasterControl::SaveCurrentProject()
{
    if (CurrentProject()->Save())
        UpdateRecentProjects(CurrentProject()->Path().CString());
}
void MasterControl::UpdateRecentProjects(QString latest)
{
    QSettings settings{};
    QStringList recentFiles{ settings.value("recentprojects").toStringList() };

    if (recentFiles.contains(latest)) {

        for (int r{0}; r < recentFiles.size(); ++r)

            if (latest == recentFiles.at(r))
                recentFiles.removeAt(r);
    }
    recentFiles.push_front(latest);

    while (recentFiles.size() > 10)
        recentFiles.pop_back();

    settings.setValue("recentprojects", recentFiles);
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}
bool MasterControl::Quit()
{
    if (!project_ || project_->Close())
        return true;
    else
        return false;
}
void MasterControl::SubscribeToEvents()
{
    if (drawDebug_)
    {
        SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(MasterControl, HandlePostRenderUpdate));
    }
}

void MasterControl::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{ (void)eventType; (void)eventData;

    scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
}

float MasterControl::Sine(const float freq, const float min, const float max, const float shift)
{
    float phase{ SinePhase(freq, shift) };
    float add{ 0.5f * (min + max) };
    return LucKey::Sine(phase) * 0.5f * (max - min) + add;
}
float MasterControl::Cosine(const float freq, const float min, const float max, const float shift)
{
    return Sine(freq, min, max, shift + 0.25f);
}
float MasterControl::SinePhase(float freq, float shift)
{
    return M_PI * 2.0f * (freq * scene_->GetElapsedTime() + shift);
}
