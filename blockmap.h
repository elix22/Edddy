/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKMAP_H
#define BLOCKMAP_H

#include <Urho3D/Urho3D.h>

#include "luckey.h"
#include "freeblock.h"
#include "history.h"

class Block;
class GridBlock;
class View3D;

typedef HashMap<IntVector2, GridBlock*> Sheet;
typedef HashMap<int, Sheet> GridMap;

class BlockMap : public QObject, public Component
{
    Q_OBJECT
    URHO3D_OBJECT(BlockMap, Component)
public:
    BlockMap(Context* context);
    void OnNodeSet(Node* node) override;
    bool Save();
    bool LoadXML(const XMLElement &source) override;
    bool SaveXML(XMLElement &dest);
    void UpdateViews() const;

    void SetFileName(const String& fileName) { fileName_ = fileName; }
    const String& GetFileName() const { return fileName_; }
    void SetName(const String& name) { name_ = name; }
    void SetSavedName(const String& savedName) { savedName_ = savedName; }
    const String& GetName() const { return name_; }
    const String& GetSavedName() const { return savedName_; }
    String GetShortName() const { return name_.Substring(0, name_.Length() - 4); }
    Scene* GetScene() const { return scene_; }
    const QColor GetBackgroundQColor() const;

    void SetMapSize(int w, int h, int d);
    void SetMapSize(IntVector3 size);
    void SetBlockSize(Vector3 size);
    void Initialize();

    IntVector3 GetMapSize() const { return mapSize_; }
    int GetMapWidth()       const { return mapSize_.x_; }
    int GetMapHeight()      const { return mapSize_.y_; }
    int GetMapDepth()       const { return mapSize_.z_; }

    Vector3 GetBlockSize() const { return blockSize_; }
    float GetBlockWidth()    const { return blockSize_.x_; }
    float GetBlockHeight()   const { return blockSize_.y_; }
    float GetBlockDepth()    const { return blockSize_.z_; }

    Vector3 GetCenter();
    BlockInstance* GetBlockInstance(IntVector3 coords, unsigned layer = 1);
    BlockInstance* GetBlockInstance(const Ray& ray) { return nullptr; }

    void SetGridBlock(IntVector3 coords, Quaternion rotation, Block* block, unsigned layer = 1);
    Vector<Block*> GetUsedBlocks();
    Vector<Blockset*> GetUsedBlocksets();
    Vector<GridBlock*> GetOccupiedGridBlocks();
    bool Contains(IntVector3 coords);

    History* GetHistory() { return history_.Get(); }
    bool Modified() const;
    StaticModelGroup* GetActiveCenterGroup()   const { return activeCenterGroup_; }
    StaticModelGroup* GetInactiveCenterGroup() const { return inactiveCenterGroup_; }
    StaticModelGroup* GetSideGroup()           const { return sideGroup_; }
    QString DisplayName() const;

private:
    String fileName_;
    String name_;
    String savedName_;
    StaticModelGroup* activeCenterGroup_;
    StaticModelGroup* inactiveCenterGroup_;
    StaticModelGroup* sideGroup_;

    HashMap<int, GridMap> gridLayers_;

    Scene* scene_;
    Zone*  zone_;
    PODVector<Node*> corners_;

    IntVector3 mapSize_;
       Vector3 blockSize_;

    SharedPtr<History> history_;

    void CreateCorners();
    int  GetBlocksetId(Block* block, HashMap<unsigned, Blockset*>& blockSetsById);
    void UpdateCorners();
};

#endif // BLOCKMAP_H
