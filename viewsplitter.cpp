/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDebug>
#include <assert.h>

#include <QPainter>
#include <QEvent>
#include <QLayout>
#include "viewsplitter.h"
#include "view3d.h"

ViewSplitter::ViewSplitter(QWidget* parent) : QSplitter(parent)
{
    setOrientation(Qt::Vertical);

    installEventFilter(this);

    connect(this, SIGNAL(splitterMoved(int,int)),
            this, SLOT(splitterMoved()));
}

bool ViewSplitter::eventFilter(QObject *watched, QEvent *event)
{

    if (event->type() == QEvent::ContextMenu && handle(count() - 1)->geometry().contains(mapFromGlobal(QCursor::pos()))) {

            setOrientation(orientation() == Qt::Horizontal ? Qt::Vertical
                                                           : Qt::Horizontal);
    }
}

void ViewSplitter::splitterMoved()
{
    for (int i: {0, 1}) {

        bool invisible{ widget(i)->visibleRegion().isEmpty() };
        if (invisible) {

            QWidget* closedWidget{ widget(i) };
            QWidget* otherWidget{ widget(!i) };

            if (ViewSplitter* parentSplitter = qobject_cast<ViewSplitter*>(parent()))
                parentSplitter->addWidget(otherWidget);
            else
                qobject_cast<QWidget*>(parent())->layout()->addWidget(otherWidget);
            otherWidget->show();

            if (View3D::Active() == qobject_cast<View3D*>(closedWidget))
                qobject_cast<View3D*>(otherWidget)->Activate();

            delete closedWidget;
            delete this;

            return;
        }
    }
}
