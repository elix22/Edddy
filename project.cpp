/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "editmaster.h"
#include "blockset.h"
#include "blockmap.h"

#include "project.h"

void Project::RegisterObject(Context* context)
{
    context->RegisterFactory<Project>();

    URHO3D_ATTRIBUTE("Name",     String, name_    , "", AM_EDIT);
    URHO3D_ATTRIBUTE("Location", String, location_, "", AM_EDIT);

    URHO3D_ATTRIBUTE_EX("ResourcesFolder", String, resourceFolder_ , OnResourcesFolderSet, "Resources/", AM_FILE);
    URHO3D_ATTRIBUTE_EX("ModelsFolder",    String, modelsFolder_   , OnFolderSet, "Models/",    AM_FILE);
    URHO3D_ATTRIBUTE_EX("MaterialsFolder", String, materialsFolder_, OnFolderSet, "Materials/", AM_FILE);
    URHO3D_ATTRIBUTE_EX("MapsFolder",      String, mapsFolder_     , OnFolderSet, "Maps/",      AM_FILE);
    URHO3D_ATTRIBUTE_EX("BlocksetsFolder", String, blocksetsFolder_, OnFolderSet, "Maps/",      AM_FILE);
}

Project::Project(Context* context) : Serializable(context),
    unsavedChanges_{true}
{
    ResetToDefault();
}
bool Project::Close()
{
    if (Modified()) {

        ModifiedBox* modifiedBox{ new ModifiedBox(context_) };
        int ret{ modifiedBox->exec() };

        switch (ret) {
        case QMessageBox::Save:

            Save();
            return true;

            break;
        case QMessageBox::Discard:

            return true;

            break;
        case QMessageBox::Cancel: default:
            return false;
            break;
        }
    }

    return true;
}
void Project::OnResourcesFolderSet()
{
    if (location_.Empty())
        return;

    if (resourceFolder_ != previousResourceFolder_) {

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };

        if (cache->GetResourceDirs().Contains(location_ + previousResourceFolder_))
            cache->RemoveResourceDir(location_ + previousResourceFolder_);

        cache->ReleaseAllResources();
        cache->AddResourceDir(location_ + resourceFolder_);
        previousResourceFolder_ = resourceFolder_;

        OnFolderSet();
    }
}
void Project::OnFolderSet()
{
    MainWindow::sidePanel()->projectPanel()->updateBrowser();
}

bool Project::Load(const String fileName)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    XMLFile* projectXML{ cache->GetResource<XMLFile>(fileName) };

    if (!projectXML)
        return false;

    if (!LoadXML(projectXML->GetRoot("project")))
        return false;

    unsavedChanges_ = false;
    return true;
}

bool Project::Save()
{
    if (!Modified())
        return true;
    
    XMLFile* projectXML{ new XMLFile(MC->GetContext()) };
    XMLElement rootElem{ projectXML->CreateRoot("project") };

    if (!SaveXML(rootElem))
        return false;

    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    File file{ MC->GetContext(), Path(), FILE_WRITE };
    projectXML->Save(file);
    file.Close();

    for (Blockset* bs: editMaster->GetBlocksets()) {

        if (bs->Modified()) {

            if (!bs->Save())
                return false;
        }
    }
    for (BlockMap* bm: editMaster->GetBlockMaps()) {

        if (bm->Modified()) {

            if (!bm->Save())
                return false;
        }
    }

    unsavedChanges_ = false;
        
    MainWindow::mainWindow_->handleFileModifiedStateChange();

    return true;
}
bool Project::Modified()
{
    bool filesModified{ false };

    for (Blockset* bs: GetSubsystem<EditMaster>()->GetBlocksets()) {

        if (bs->Modified()) {

            filesModified = true;
            break;
        }
    }

    if (!filesModified) {

        for (BlockMap* bm: GetSubsystem<EditMaster>()->GetBlockMaps()) {

            if (bm->Modified()) {

                filesModified = true;
                break;
            }
        }
    }

    return unsavedChanges_ || filesModified;
}


String Project::Location()
{
    return location_;
}
String Project::Path()
{
    return location_ + name_;
}
String Project::GetFolder(const String& folderName)
{
    String folder{ GetAttribute(folderName + "Folder").GetString() };
    return folder;
}
QString Project::TrimmedName()
{
    QString name{ name_.CString() };
    name.chop(4);
    return name;
}

ModifiedBox::ModifiedBox(Context* context, QWidget *parent): QMessageBox(parent), Object(context)
{
    EditMaster* editMaster{ GetSubsystem<EditMaster>() };

    setWindowTitle("Unsaved changes");
    setWindowIcon(QIcon(":/Edddy"));
    setIcon(QMessageBox::Information);
    setText("The current project has been modified.");
    setInformativeText("Do you want to save your changes?");
    setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    setDefaultButton(QMessageBox::Save);
    QString changeList{ "Modified files:\n" };

    for (BlockMap* bm: editMaster->GetBlockMaps()) {

        if (bm->Modified()) {

            changeList.append(bm->GetName().CString());
            changeList.append('\n');
        }
    }
    for (Blockset* bs: editMaster->GetBlocksets()) {

        if (bs->Modified()) {

            changeList.append(bs->name_.CString());
            changeList.append('\n');
        }
    }

    setDetailedText(changeList);
}
