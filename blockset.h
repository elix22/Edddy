/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKSET_H
#define BLOCKSET_H

#include <Urho3D/Urho3D.h>
#include "luckey.h"

#include "block.h"

#define NOID M_MAX_UNSIGNED

class Blockset : public QObject, public Serializable
{
    Q_OBJECT
    URHO3D_OBJECT(Blockset, Serializable);
public:
    Blockset(Context* context);

    bool blocksChanged_;
    String name_;
    String savedName_;

    QString DisplayName() const;

    Vector<Block*> blocks_;
    Block* GetBlockById(unsigned id);

    Block** begin() { return &blocks_[0]; }
    Block** end()   { return blocks_.End().ptr_; }

    bool Modified() const;
    bool LoadXML(const XMLElement& source);
    bool Save();
    void AddBlock(String blockName, String modelName, Materials materials, unsigned id = NOID);
    void RemoveBlock(Block* block);
};

#endif // BLOCKSET_H
