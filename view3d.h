/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VIEW3D_H
#define VIEW3D_H

#include <QWidget>
#include "luckey.h"

class QComboBox;

class View3D : public QWidget, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(View3D, Object)

public:
    explicit View3D(Context* context);
    ~View3D();

    static Vector<View3D*> views_;

    void SetMap(BlockMap* blockMap);
    BlockMap* GetBlockMap() const { return blockMap_; }

    void CreateCamera(StringHash, VariantMap&);
    void CreateCamera();
    void UpdateView();
    void UpdateView(StringHash, VariantMap&);
    void CreateRenderTexture();
    void UpdateMapBox();
    static View3D* Active() { return active_; }

    void Activate();

    void Split();
public slots:
    void SetMap(int index);
protected:
    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;

private:
    void PaintView();
    void PaintView(const StringHash, VariantMap&);

    static View3D* active_;

    QComboBox* mapBox_;
    QPoint previousMousePos_;

    SharedPtr<Texture2D> renderTexture_;
    SharedPtr<Image> image_;
    QPixmap pixmap_;
    SharedPtr<Node> cameraNode_;
    EdddyCam* edddyCam_;
    BlockMap* blockMap_;

    void UpdateViewport();
    QPixmap ImageToPixmap(Image* image);
    void PaintBorder();
    void UpdatePixmap();
    void HandleCursorStep(StringHash, VariantMap&);
};

#endif // VIEW3D_H
