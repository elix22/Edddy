/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GRIDBLOCK_H
#define GRIDBLOCK_H

#include <Urho3D/Urho3D.h>

#include "blockinstance.h"

class GridBlock : public BlockInstance
{
    URHO3D_OBJECT(GridBlock, BlockInstance);
public:
    GridBlock(Context* context);
    void OnNodeSet(Node* node) override;

    bool WithinLock();
    IntVector3 GetCoords() const { return coords_; }
    unsigned GetLayer() const { return layer_; }

    void Init(IntVector3 coords, unsigned layer = 0);
private:
    bool centerDirty_;
    IntVector3 coords_;
    unsigned layer_;

    Node* sidesNode_;
    Node* centerNode_;
    Vector<Node*> sideNodes_;
    StaticModel* centerModel_;
    //Called by BlockMap
    void SetCoords(IntVector3 coords);
    void UpdatePosition();
    void CreateSideNodes();
    void UpdateCenter(StringHash, VariantMap&);
};

#endif // GRIDBLOCK_H
