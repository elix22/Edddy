/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "edddycursor.h"
#include "gridblock.h"
#include "editmaster.h"
#include "blockset.h"
#include "view3d.h"
#include "project.h"

#include "blockmap.h"

BlockMap::BlockMap(Context* context) : QObject(), Component(context),
    name_{},
    savedName_{},
    gridLayers_{},
    scene_{},
    history_{new History(context)}
{
}
const QColor BlockMap::GetBackgroundQColor() const
{
    Color color{ zone_->GetFogColor() };
    QColor backgroundColor{ color.r_ * 255, color.g_ * 255, color.b_ * 255 };

    return backgroundColor;
}

void BlockMap::UpdateViews() const
{
    for (View3D* v: View3D::views_) {
        if (v->GetBlockMap() == this) {

            v->UpdateView();
        }
    }
}
void BlockMap::OnNodeSet(Node *node)
{ if (!node) return;

    scene_ = static_cast<Scene*>(node);
    scene_->CreateComponent<Octree>();

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    activeCenterGroup_ = scene_->CreateComponent<StaticModelGroup>();
    activeCenterGroup_->SetModel(cache->GetTempResource<Model>("Models/Center.mdl"));
    activeCenterGroup_->SetMaterial(cache->GetTempResource<Material>("Materials/CenterActive.xml"));

    inactiveCenterGroup_ = scene_->CreateComponent<StaticModelGroup>();
    inactiveCenterGroup_->SetModel(cache->GetTempResource<Model>("Models/Center.mdl"));
    inactiveCenterGroup_->SetMaterial(cache->GetTempResource<Material>("Materials/CenterInactive.xml"));

    sideGroup_ = scene_->CreateComponent<StaticModelGroup>();
    sideGroup_->SetModel(cache->GetTempResource<Model>("Models/Plane.mdl"));
    sideGroup_->SetMaterial(cache->GetTempResource<Material>("Materials/TransparentGlow.xml"));

    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(-0.5f, 5.0f, -2.0f));
    lightNode->LookAt(Vector3::ZERO);
    Light* previewLight{ lightNode->CreateComponent<Light>() };
    previewLight->SetLightType(LIGHT_DIRECTIONAL);

    zone_ = node_->CreateComponent<Zone>();
    zone_->SetBoundingBox(BoundingBox(-Vector3::ONE * 10e9, Vector3::ONE * 10e9));
    zone_->SetAmbientColor(Color(0.13f, 0.23f, 0.42f));
    zone_->SetFogStart(2048.0f);
    zone_->SetFogEnd(4096.0f);
    zone_->SetFogColor(Color::BLACK);

    CreateCorners();
}

void BlockMap::Initialize()
{
    for (int l{1}; l <= 5; ++l) {
        GridMap gridMap{};

        for (int y{0}; y < GetMapHeight(); ++y) {
            Sheet sheet{};

            for (int x{0}; x < GetMapWidth(); ++x)
                for (int z{0}; z < GetMapDepth(); ++z) {
                    Node* gridNode{ node_->CreateChild("GridBlock") };
                    GridBlock* gridBlock{ gridNode->CreateComponent<GridBlock>() };
                    gridBlock->Init(IntVector3(x, y, z), l);

                    IntVector2 sheetCoords(x, z);
                    sheet[sheetCoords] = gridBlock;
                }
            gridMap[y] = sheet;
        }
        gridLayers_[l] = gridMap;
    }
}

void BlockMap::CreateCorners()
{
    for (int c{0}; c < 8; ++c) {

        Node* cornerNode{ scene_->CreateChild("MapCorner") };

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        StaticModel* cornerModel{ cornerNode->CreateComponent<StaticModel>() };
        cornerModel->SetModel(cache->GetTempResource<Model>("Models/Corner.mdl"));
        cornerModel->SetMaterial(cache->GetTempResource<Material>("Materials/CornerInactive.xml"));

        corners_.Push(cornerNode);
    }
}
void BlockMap::UpdateCorners()
{
    for (unsigned i{0}; i < corners_.Size(); ++i) {

        Node* c{ corners_.At(i) };
        c->SetPosition(Vector3(-0.5f + 1.0f *  (i % 2),
                               -0.5f + 1.0f * ((i / 2) % 2),
                               -0.5f + 1.0f *  (i / 4))
                       * Vector3(mapSize_) * blockSize_ + GetCenter() - 0.5f * blockSize_);

        Vector3 pos{ c->GetPosition() };
        c->SetScale(blockSize_ * Vector3(Sign(pos.x_) * Sign(pos.y_) * Sign(pos.z_), 1.0f, 1.0f));
        c->LookAt(Vector3::FORWARD * pos.z_, Vector3::UP * pos.y_, TS_LOCAL);
    }
}

bool BlockMap::LoadXML(const XMLElement &source)
{
    if (!Serializable::LoadXML(source))
        return false;

    SetMapSize(source.GetIntVector3("map_size"));
    SetBlockSize(source.GetVector3("block_size"));
    Initialize();

    XMLElement blockSetXML{ source.GetChild("blockset") };

    while (blockSetXML.NotNull()) {

        Blockset* blockSet{ GetSubsystem<EditMaster>()->LoadBlockset(blockSetXML.GetAttribute("name")) };
        if (blockSet) {

            XMLElement layerXML{ source.GetChild("gridlayer") };
            if (layerXML.IsNull())
                continue;

            while (layerXML.NotNull()) {
                unsigned layer{ layerXML.GetUInt("id")};

                XMLElement gridBlockXML{ layerXML.GetChild("gridblock") };
                while (gridBlockXML.NotNull()) {

                    if (gridBlockXML.GetInt("set") == blockSetXML.GetInt("id")){

                        SetGridBlock(gridBlockXML.GetIntVector3("coords"),
                                     gridBlockXML.GetQuaternion("rot"),
                                     blockSet->GetBlockById(gridBlockXML.GetUInt("block")),
                                     layer);

                    }

                    gridBlockXML = gridBlockXML.GetNext("gridblock");
                }

                layerXML = layerXML.GetNext("gridlayer");
            }
        }

        blockSetXML = blockSetXML.GetNext("blockset");
    }

    return true;
}

bool BlockMap::Save()
{
    for (Blockset* bs: GetUsedBlocksets()) {

        if (!bs->Save())
            return false;
    }

    if (!Modified())
        return true;


    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    XMLFile* mapXML{ cache->GetResource<XMLFile>(fileName_) };

    if (!mapXML)
        mapXML = new XMLFile(context_);

    XMLElement rootElem{ mapXML->CreateRoot("blockmap") };

    if (!SaveXML(rootElem))
        return false;

    Project* project{ GetSubsystem<MasterControl>()->CurrentProject() };
    String fileName{ project->Location() + project->GetFolder("Resources") + project->GetFolder("Maps") + name_ };

    if (mapXML->SaveFile(fileName)) {

        history_->HandleSave();

        if (fileName_ != fileName) {


            FileSystem* fs{ GetSubsystem<FileSystem>() };
            fs->Delete(fileName_);
            fileName_ = fileName;
        }
        savedName_ = name_;

        return true;
    }

    return false;
}
bool BlockMap::SaveXML(XMLElement& dest)
{
    dest.SetIntVector3("map_size", GetMapSize());
    dest.SetVector3("block_size", GetBlockSize());

    HashMap<unsigned, Blockset*> blockSetsById{};
    unsigned blocksetId{ 0 };

    for (Blockset* blockSet : GetUsedBlocksets()) {

        XMLElement blockSetElem{ dest.CreateChild("blockset") };
        blockSetsById[blocksetId] = blockSet;
        blockSetElem.SetInt("id", blocksetId);
        ++blocksetId;

        blockSetElem.SetAttribute("name", blockSet->savedName_);
    }

    HashMap<unsigned, XMLElement> layerElements{};
    for (int l{1}; l <= 5; ++l) {

        layerElements[l] = dest.CreateChild("gridlayer");
        layerElements[l].SetUInt("id", l);
    }

    for (GridBlock* gridBlock : GetOccupiedGridBlocks()) {

        Block* block{ gridBlock->GetBlock() };
        int blocksetId{ GetBlocksetId(block, blockSetsById) };

        if (blocksetId >= 0) {

            IntVector3 coords{ gridBlock->GetCoords() };
            XMLElement gridBlockElem{ layerElements[gridBlock->GetLayer()].CreateChild("gridblock") };

            gridBlockElem.SetUInt("set", blocksetId);
            gridBlockElem.SetUInt("block", block->Id());
            gridBlockElem.SetIntVector3("coords", coords);
            gridBlockElem.SetQuaternion("rot", gridBlock->GetRotation());
        }
    }

    for (const XMLElement& layerElement: layerElements.Values()) {

        if (!layerElement.HasChild("gridblock"))
            dest.RemoveChild(layerElement);
    }

    return true;
}
int BlockMap::GetBlocksetId(Block* block, HashMap<unsigned, Blockset*>& blockSetsById)
{
    for (unsigned id : blockSetsById.Keys()) {

        if (blockSetsById[id]->blocks_.Contains(block))

            return id;
    }

    return -1;
}

Vector<Block*> BlockMap::GetUsedBlocks()
{
    Vector<Block*> blocks{};
    for (GridMap& layer: gridLayers_.Values()) {
        for (Sheet& sheet: layer.Values()) {

            for (GridBlock* gridBlock: sheet.Values()) {

                Block* block{ gridBlock->GetBlock() };
                if (block && !blocks.Contains(block))
                    blocks.Push(block);
            }
        }
    }
    return blocks;
}
Vector<Blockset*> BlockMap::GetUsedBlocksets()
{
    Vector<Blockset*> blockSets{};

    for (Block* block : GetUsedBlocks()) {
        Blockset* blockSet{ block->GetBlockset() };

        if (blockSet && !blockSets.Contains(blockSet)) {

            blockSets.Push(blockSet);
        }
    }
    return blockSets;
}
Vector<GridBlock*> BlockMap::GetOccupiedGridBlocks()
{
    Vector<GridBlock*> gridBlocks{};
    for (GridMap& layer: gridLayers_.Values()) {
        for (Sheet& sheet: layer.Values()) {
            for (GridBlock* gridBlock: sheet.Values()) {
                if (gridBlock->GetBlock() != nullptr)

                    gridBlocks.Push(gridBlock);
            }
        }
    }
    return gridBlocks;
}

BlockInstance* BlockMap::GetBlockInstance(IntVector3 coords, unsigned layer)
{
    BlockInstance* blockInstance{ nullptr };
    Sheet sheet{};

    if (gridLayers_[layer].TryGetValue(coords.y_, sheet)) {

        GridBlock* gridBlock{ nullptr };
        if (sheet.TryGetValue(IntVector2(coords.x_, coords.z_), gridBlock))
            blockInstance = static_cast<BlockInstance*>(gridBlock);
    }

    return blockInstance;
}

void BlockMap::SetGridBlock(IntVector3 coords, Quaternion rotation, Block* block, unsigned layer)
{

    Sheet sheet{};
    if (gridLayers_[layer].TryGetValue(coords.y_, sheet)){

        GridBlock* gridBlock{ nullptr };
        if (sheet.TryGetValue(IntVector2(coords.x_, coords.z_), gridBlock))
            gridBlock->SetBlock(block, rotation);
    }
}

void BlockMap::SetBlockSize(Vector3 size)
{
    blockSize_ = size;

    UpdateCorners();
}
void BlockMap::SetMapSize(IntVector3 size)
{
    mapSize_ = size;

    UpdateCorners();
}
void BlockMap::SetMapSize(int w, int h, int d)
{
    SetMapSize(IntVector3(w, h, d));
}

Vector3 BlockMap::GetCenter()
{
    return 0.5f * blockSize_ * Vector3(mapSize_.x_, mapSize_.y_, mapSize_.z_);
}
bool BlockMap::Contains(IntVector3 coords)
{
    return (coords.x_ >= 0 && coords.x_ < mapSize_.x_
         && coords.y_ >= 0 && coords.y_ < mapSize_.y_
            && coords.z_ >= 0 && coords.z_ < mapSize_.z_);
}

bool BlockMap::Modified() const
{
    return savedName_ != name_ || history_->Modified();
}
QString BlockMap::DisplayName() const
{
    QString displayName{ name_.CString() };

    if (Modified())
        displayName.append('*');

    return displayName;
}
