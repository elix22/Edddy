![Edddy](banner.svg)


**Edddy** is a block based 3D map editor built on [Urho3D](http://urho3d.github.io). Its purpose is to create levels for [Blip 'n Blup](https://gitlab.com/LucKeyProductions/BlipNBlup), [A-Mazing Urho](https://gitlab.com/luckeyproductions/AmazingUrho), [OG Tatt](https://gitlab.com/luckeyproductions/OGTatt), [KO](https://gitlab.com/luckeyproductions/KO) and [Octalloc](https://gitlab.com/luckeyproductions/Octalloc).

The [Urho-Blender exporter](https://github.com/reattiva/Urho3D-Blender) and [TiNA](https://gitlab.com/Modanung/TiNA/) both assist in creating suitable assets.

![Screenshot](Screenshots/Screenshot_2018-11-11_00-40-25.png)
