/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROJECTPANEL_H
#define PROJECTPANEL_H

#include <QTreeWidget>
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

#include "luckey.h"

class ProjectPanel : public QWidget, Object
{
    Q_OBJECT
    URHO3D_OBJECT(ProjectPanel, Object)
public:
    explicit ProjectPanel(Context* context, QWidget *parent = nullptr);

    void updatePanel();
    void updateBrowser();
private slots:
    void onFolderEditFinished();
    void onFolderResetClicked();
    void resourceBrowserItemActivated(QTreeWidgetItem* item);
    void showBrowserMenu(const QPoint& pos);

    void errorCatcher() {} ///
    void deleteSelectedFile();
    void onFolderPickClicked();
private:
    void storeExpandedFolders(StringVector& expanded);
    void editBrowserItem();

    QLabel* projectNameLabel_;

    QLineEdit* resourcesEdit_;
    QLineEdit* modelsEdit_;
    QLineEdit* materialsEdit_;
    QLineEdit* mapsEdit_;
    QLineEdit* blocksetsEdit_;

    QPushButton* resourcesResetButton_;
    QPushButton* resetModelsButton_;
    QPushButton* resetMaterialsButton_;
    QPushButton* resetMapsButton_;
    QPushButton* resetBlocksetsButton_;

    QTreeWidget* resourceBrowser_;

    std::vector<QLineEdit*> folderEdits_;
    const QStringList folders{"Models", "Materials", "Maps", "Blocksets"};
};

#endif // PROJECTPANEL_H
