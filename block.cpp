/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "editmaster.h"
#include "blockset.h"

#include "mainwindow.h"

#include "block.h"

void Block::RegisterAttributes(Context* context)
{
    URHO3D_ATTRIBUTE("id", unsigned, id_, 0, AM_FILE);
    URHO3D_ATTRIBUTE("name", String, name_, "", AM_FILE);
//    URHO3D_ATTRIBUTE("material", String, material_, "", AM_FILE);
}

Block::Block(Context* context) : QObject(), Serializable(context),
    id_{ NOID },
    name_{ "" },
    materials_{},
    previewScene_{ new Scene(context) },
    previewNode_{},
    previewModel_{},
    image_{},
    renderTexture_{ new Texture2D(context) }
{
    CreatePreviewRenderer();
}
void Block::SetModel(Model* model)
{
    previewModel_->SetModel(model);
    applyMaterials();
}
void Block::SetMaterial(Material* material)
{
    SetMaterial(SINGLE, material);
}
void Block::SetMaterial(unsigned index, Material* material)
{
    if (index == SINGLE)
        materials_.Clear();

    else {
        if (index > numGeometries() - 1)
            return;

        else if (materials_.Contains(SINGLE)) {
            
            for (unsigned m{ 0 }; m < numGeometries(); ++m) {

                materials_[m] = materials_[SINGLE];
            }

            materials_.Erase(SINGLE);
        }
    }
    
    if (material)

        materials_[index] = material->GetName();

    else

        materials_.Erase(index);

    applyMaterials();
}
void Block::SetMaterials(const Materials& materials)
{
    if (materials == materials_)
        return;

    materials_ = materials;

    applyMaterials();
}
Model* Block::model() const
{
    return previewModel_->GetModel();
}

String Block::modelName() const
{
    if (model())
        return model()->GetName();

    else
        return "";
}
unsigned Block::numGeometries()
{
    if (model())
        return model()->GetNumGeometries();

    else
        return 0;
}

void Block::applyMaterials()
{
    if (materials_.Contains(SINGLE)) {

        previewModel_->SetMaterial(material(SINGLE));

    } else for (unsigned g{0}; g < numGeometries(); ++g) {

        if (materials_.Contains(g))

            previewModel_->SetMaterial(g, material(g));

        else

            previewModel_->SetMaterial(g, nullptr);
    }

    UpdatePreview();
}
Material* Block::material() const
{
    return material(SINGLE);
}
Material* Block::material(unsigned index) const
{
    if (materials_.Contains(index)) {

        return GetSubsystem<ResourceCache>()->GetResource<Material>(*materials_[index]);

    } else {

        return nullptr;
    }
}

Blockset* Block::GetBlockset()
{
    for (Blockset* bs : GetSubsystem<EditMaster>()->GetBlocksets()) {

        if (bs->blocks_.Contains(this))
            return bs;
    }
    return nullptr;
}

void Block::SaveXML(XMLElement &dest)
{
    XMLElement blockElem{ dest.CreateChild("block") };

    blockElem.SetUInt("id", id_);
    blockElem.SetString("name", name_);
    blockElem.SetString("model", modelName());

    if (materials_.Size()) {

        if (materials_.Keys().Front() == SINGLE)

            blockElem.SetString("material", materials_.Values().Front());

        else {
            for (unsigned id: materials_.Keys()) {

                XMLElement materialsElement{ blockElem.CreateChild("material") };
                materialsElement.SetUInt("index", id);
                materialsElement.SetString("name", materials_[id]);
            }
        }
    }
}

void Block::CreatePreviewRenderer()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    previewScene_->CreateComponent<Octree>();

    Node* lightNode{ previewScene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(-0.5f, 5.0f, -2.0f));
    lightNode->LookAt(Vector3::ZERO);
    Light* previewLight{ lightNode->CreateComponent<Light>() };
    previewLight->SetLightType(LIGHT_DIRECTIONAL);

    Node* cameraNode{ previewScene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3(0.0f, 2.0f, -3.0f));
    cameraNode->LookAt(Vector3::UP * 0.125f);
    Camera* previewCamera{ cameraNode->CreateComponent<Camera>() };
    previewCamera->SetFov(23.0f);

    previewNode_ = previewScene_->CreateChild("Preview");
    previewModel_ = previewNode_->CreateComponent<StaticModel>();
//    previewModel_->SetModel(cache->GetResource<Model>("Models/Random.mdl"));
//    previewModel_->SetMaterial(cache->GetResource<Material>("Materials/VCol.xml"));

    renderTexture_->SetSize(128, 128, Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };
    renderSurface->SetUpdateMode(SURFACE_MANUALUPDATE);
    RenderPath* renderPath{ new RenderPath() };
    renderPath->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml"));
    SharedPtr<Viewport> viewport{ new Viewport(context_, previewScene_, previewCamera, renderPath)};
    renderSurface->SetViewport(0, viewport);
}

void Block::UpdatePreview()
{
    previewNode_->SetRotation(Quaternion(MainWindow::sidePanel()->blocksPanel()->rotationSliderValue(), Vector3::DOWN));

    renderTexture_->GetRenderSurface()->QueueUpdate();
    GetSubsystem<MasterControl>()->RequestUpdate();
}
