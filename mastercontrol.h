/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include <QApplication>

#include "luckey.h"
#include "edddyevents.h"

using namespace Urho3D;

class EffectMaster;
class InputMaster;
class EdddyCam;
class BlockMap;
class MainWindow;
class Project;


class MasterControl : public QApplication, public Application
{
    Q_OBJECT
    URHO3D_OBJECT(MasterControl, Application);
public:
    MasterControl(int & argc, char** argv, Context* context);
    virtual ~MasterControl();

    void Setup() override;
    void Start() override;
    void Stop() override;
    void Exit();

    float Sine(const float freq, const float min, const float max, const float shift = 0.0f);
    float Cosine(const float freq, const float min, const float max, const float shift = 0.0f);

    void RunFrame();

    void NewProject(QString path);
    void OpenProject(String path);
    void SaveCurrentProject();
    Project* CurrentProject() { return project_.Get(); }
public slots:
    void OpenRecentProject() { OpenProject(sender()->objectName().toStdString().data()); }
    void RequestUpdate() { requireUpdate_ = true; }
    void OnTimeout();
    bool Quit();
private:
    void SubscribeToEvents();
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    float SinePhase(float freq, float shift);

    template <class T> void RegisterFactory() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }

    String argument_;
    MainWindow* mainWindow_;

    SharedPtr<UI> ui_;
    SharedPtr<Renderer> renderer_;
    SharedPtr<XMLFile> defaultStyle_;
    SharedPtr<Project> project_;

    SharedPtr<Scene> scene_;
    bool requireUpdate_;
    bool drawDebug_;
    void UpdateRecentProjects(QString latest);
};

#endif // MASTERCONTROL_H
