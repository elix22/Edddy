/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// Commercial licenses are available through frode@lindeijer.nl
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCK_H
#define BLOCK_H

#include <Urho3D/Urho3D.h>
#include "luckey.h"

class Blockset;

URHO3D_EVENT(E_BLOCKCHANGED, BlockChanged){}


#define SINGLE M_MAX_UNSIGNED
using Materials = HashMap<unsigned, String>;

class Block : public QObject, public Serializable
{
    friend class EditMaster;

    Q_OBJECT
    URHO3D_OBJECT(Block, Serializable);
public:
    Block(Context* context);
    static void RegisterAttributes(Context *context);

    void SetId(unsigned id) { id_ = id;  }
    void SetName(String name) { name_ = name; }
    void SetModel(Model* model);
    void SetMaterial(unsigned index, Material* material);
    void SetMaterial(Material* material);
    void SetMaterials(const Materials& materials);

    unsigned Id() const { return id_; }
    String name() const { return name_; }

    Model* model() const;
    String modelName() const;
    unsigned numGeometries();

    Material* material() const;
    Material* material(unsigned index) const;
    const Materials& materials() const { return materials_; }

    Image* image() { image_ = renderTexture_->GetImage(); return image_;}

    Blockset* GetBlockset();

    void SaveXML(XMLElement& dest);
    void UpdatePreview();
private:
    unsigned id_;
    String name_;
    Materials materials_;

    Scene* previewScene_;
    Node* previewNode_;
    StaticModel* previewModel_;

    SharedPtr<Image> image_;
    SharedPtr<Texture2D> renderTexture_;

    void CreatePreviewRenderer();
    void applyMaterials();
};

#endif // BLOCK_H
