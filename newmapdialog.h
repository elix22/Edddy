/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NEWMAPDIALOG_H
#define NEWMAPDIALOG_H

#include "luckey.h"
#include <QDialog>

#include <QKeyEvent>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDialogButtonBox>

class ShortDoubleSpinBox: public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit ShortDoubleSpinBox(QWidget *parent = nullptr): QDoubleSpinBox(parent) {}
    QString textFromValue(double value) const override
    {
        return QLocale().toString(value, 'g', QLocale::FloatingPointShortest);
    }
    void keyPressEvent(QKeyEvent *e) override
    {
        if (e->key() == Qt::Key_Period && QLocale().decimalPoint() == ',') {

            QKeyEvent* p{ new QKeyEvent(QEvent::KeyPress,   Qt::Key_Comma, Qt::NoModifier, ",") };
            QKeyEvent* r{ new QKeyEvent(QEvent::KeyRelease, Qt::Key_Comma, Qt::NoModifier, ",") };

            qApp->postEvent(this, p);
            qApp->postEvent(this, r);

            return;
        }
        QAbstractSpinBox::keyPressEvent(e);
    }
};

class NewMapDialog : public QDialog, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(ShortDoubleSpinBox, Object)
public:
    explicit NewMapDialog(Context* context, QWidget *parent = nullptr);
public slots:
    void accept() override;
private slots:
    void nameEdited();
    void nameEditFinished();
private:
    static IntVector3 mapSizeDefault_;
    static Vector3 blockSizeDefault_;
    QLineEdit* nameEdit_;
    QDialogButtonBox* buttonBox_;

    QSpinBox* mapWidthBox_;
    QSpinBox* mapHeightBox_;
    QSpinBox* mapDepthBox_;

    ShortDoubleSpinBox* blockWidthBox_;
    ShortDoubleSpinBox* blockHeightBox_;
    ShortDoubleSpinBox* blockDepthBox_;
};

#endif // NEWMAPDIALOG_H
