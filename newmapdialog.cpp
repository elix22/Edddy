/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "editmaster.h"
#include "blockmap.h"
#include "view3d.h"
#include "mainwindow.h"

#include "newmapdialog.h"
IntVector3 NewMapDialog::mapSizeDefault_{ 23, 5, 23};
Vector3 NewMapDialog::blockSizeDefault_{ 1.0f, 1.0f, 1.0f };

NewMapDialog::NewMapDialog(Context* context, QWidget *parent) : QDialog(parent), Object(context),
    nameEdit_{ new QLineEdit() },
    buttonBox_{ new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel) }
{
    setWindowTitle("New Map");
    setWindowIcon(QIcon(":/Map"));

    BlockMap* activeMap{ nullptr };
    if (View3D::Active()) {

        activeMap = View3D::Active()->GetBlockMap();
    }

    QVBoxLayout* layout{ new QVBoxLayout(this) };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout() };
    form->setLabelAlignment(Qt::AlignRight);

    form->addRow("Name:", nameEdit_);

    QHBoxLayout* mapRow{ new QHBoxLayout() };
    for (int i{0}; i < 3; ++i) {

        QSpinBox* spinBox{ new QSpinBox() };
        QString labelText{};

        switch (i) {
        case 0:
            labelText = "X";
            mapWidthBox_ = spinBox;
            spinBox->setValue(activeMap ? activeMap->GetMapWidth()  : mapSizeDefault_.x_ );
            break;
        case 1:
            labelText = "Y";
            mapHeightBox_ = spinBox;
            spinBox->setValue(activeMap ? activeMap->GetMapHeight() : mapSizeDefault_.y_ );
            break;
        case 2:
            labelText = "Z";
            mapDepthBox_ = spinBox;
            spinBox->setValue(activeMap ? activeMap->GetMapDepth()  : mapSizeDefault_.z_ );
            break;
        default:
            break;
        }
        mapRow->addWidget(new QLabel(labelText));

        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        mapRow->addWidget(spinBox);
    }
    form->addRow("Map Size:", mapRow);

    QHBoxLayout* blockRow{ new QHBoxLayout() };
    for (int i{0}; i < 3; ++i) {

        ShortDoubleSpinBox* doubleSpinBox{ new ShortDoubleSpinBox() };
        QString labelText{};

        switch (i) {
        case 0:
            labelText = "X";
            blockWidthBox_ = doubleSpinBox;
            doubleSpinBox->setValue(activeMap ? activeMap->GetBlockWidth() : blockSizeDefault_.x_ );
            break;
        case 1:
            labelText = "Y";
            blockHeightBox_ = doubleSpinBox;
            doubleSpinBox->setValue(activeMap ? activeMap->GetBlockHeight() : blockSizeDefault_.y_ );
            break;
        case 2:
            labelText = "Z";
            blockDepthBox_ = doubleSpinBox;
            doubleSpinBox->setValue(activeMap ? activeMap->GetBlockDepth() : blockSizeDefault_.z_ );
            break;
        default:
            break;
        }
        blockRow->addWidget(new QLabel(labelText));

        doubleSpinBox->setDecimals(3);
        blockRow->addWidget(doubleSpinBox);
    }
    form->addRow("Grid Size:", blockRow);

    QPushButton* okButton{ buttonBox_->button(QDialogButtonBox::Ok) };
    okButton->setEnabled(false);
    okButton->setAutoDefault(false);
    okButton->setDefault(false);
    layout->addLayout(form);
    layout->addWidget(buttonBox_);

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    connect(nameEdit_, SIGNAL(textEdited(QString)), this, SLOT(nameEdited()));
    connect(nameEdit_, SIGNAL(editingFinished()),   this, SLOT(nameEditFinished()));

    exec();
}

void NewMapDialog::accept()
{
    if (!buttonBox_->button(QDialogButtonBox::Ok)->hasFocus())
        return;

    QString name{ nameEdit_->text() };

    IntVector3 mapSize{ mapWidthBox_->value(),
                        mapHeightBox_->value(),
                        mapDepthBox_->value() };

    Vector3 blockSize{ static_cast<float>(blockWidthBox_->value()),
                       static_cast<float>(blockHeightBox_->value()),
                       static_cast<float>(blockDepthBox_->value()) };



    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    BlockMap* newMap{ editMaster->NewMap(mapSize, blockSize, name.toStdString().data()) };
    editMaster->OpenMap(newMap);
    MainWindow::sidePanel()->projectPanel()->updateBrowser();
    MainWindow::mainWindow_->updateWindowTitle();

    QDialog::accept();
}

void NewMapDialog::nameEdited()
{
    if (nameEdit_->text().isEmpty()) {

        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(false);

    } else {

        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}
void NewMapDialog::nameEditFinished()
{
    QString text{ nameEdit_->text() };

    if (text.contains('.')) {

        text = text.split('.').first();
    }
    if (text.contains('/')) {
        text.replace('/', "");
    }
    if (!text.isEmpty())
        text.append(".emp");

    EditMaster* editMaster{ GetSubsystem<EditMaster>() };
    for (BlockMap* bm: editMaster->GetBlockMaps()) { ///Only includes open maps!

        if (bm->GetName().CString() == text)
            text.clear();
    }

    if (text != nameEdit_->text()) {

        nameEdit_->setText(text);
        nameEdited();
    }
}
