/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QListWidget>
#include <QListWidgetItem>
#include <QRadioButton>
#include <QScreen>

#include "editmaster.h"
#include "blockset.h"
#include "mainwindow.h"

#include "blockdialog.h"

BlockDialog::BlockDialog(Context* context, Vector<Block*> blocks, QWidget* parent) : QDialog(parent), Object(context),
    blocksIn_{        blocks },
    previewBlocks_{},
    previewItems_{},
    splitter_{        new QSplitter() },
    previewList_{     new QListWidget() },
    modelList_{       new QListWidget() },
    materialList_{    new QListWidget() },
    materialLabel_{   new QLabel("Indices:") },
    materialButtons_{ new QWidget() },
    buttonBox_{       new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel) }
{
    QString title{};

    switch (blocksIn_.Size()) {
     case 0: title = "New Block"  ; break;
     case 1: title = "Edit Block" ; break;
    default: title = "Edit Blocks"; break;
    }
    setWindowTitle(title);
    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMaximizeButtonHint);

    QVBoxLayout* leftLayout{ new QVBoxLayout() };
    leftLayout->setMargin(0);

    int lineHeight{ 16 };
    int previewIconSize{ 128 };
//    blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    previewList_->setMinimumSize(previewIconSize + lineHeight, previewIconSize + lineHeight);
    previewList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    previewList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
    previewList_->setMovement(QListWidget::Static);
    previewList_->setFlow(QListWidget::LeftToRight);
    previewList_->setWrapping(true);
    previewList_->setResizeMode(QListWidget::Adjust);
    previewList_->setSpacing(0);
    previewList_->setGridSize(QSize(previewIconSize, previewIconSize + lineHeight));
    previewList_->setIconSize(QSize(previewIconSize, previewIconSize));
    previewList_->setViewMode(QListWidget::IconMode);
    previewList_->setUniformItemSizes(true);
    previewList_->setWordWrap(true);
    previewList_->setSelectionRectVisible(true);
    previewList_->setSelectionMode(QListWidget::NoSelection);

    if (blocksIn_.Size()) {

        if (blocksIn_.Size() > 1)
            previewList_->setSelectionMode(QListWidget::ExtendedSelection);

        for (Block* b: blocksIn_) {

            Block* previewBlock{ new Block(context_) };
            previewBlock->SetModel(b->model());
            previewBlock->SetMaterials(b->materials());
            previewBlocks_.Push(previewBlock);
            BlockListItem* previewItem{ new BlockListItem(context_, previewBlock)};
            previewItem->setSizeHint(QSize(previewIconSize, previewIconSize + lineHeight));
            previewItem->setData(PointerRole, QVariant(QMetaType::QObjectStar, &previewBlock));
            previewItems_.Push(previewItem);
            previewList_->addItem(previewItem);
            previewItem->setSelected(true);
        }
    } else {

        Block* previewBlock{ new Block(context_) };
        previewBlocks_.Push(previewBlock);
        BlockListItem* previewItem{ new BlockListItem(context_, previewBlock)};
        previewItem->setSizeHint(QSize(previewIconSize, previewIconSize + lineHeight));
        previewItem->setData(PointerRole, QVariant(QMetaType::QObjectStar, &previewBlock));
        previewItems_.Push(previewItem);
        previewList_->addItem(previewItem);
    }

    leftLayout->addWidget(previewList_);
    QWidget* leftPanel{ new QWidget() };
    leftPanel->setLayout(leftLayout);
    splitter_->addWidget(leftPanel);

    QVBoxLayout* rightLayout{ new QVBoxLayout() };
    rightLayout->setMargin(0);

    int resourceIconSize{ 96 };
    for (QListWidget* resourceList: {modelList_, materialList_}) {

        resourceList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        resourceList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        resourceList->setMovement(QListWidget::Static);
        resourceList->setFlow(QListWidget::LeftToRight);
        resourceList->setWrapping(true);
        resourceList->setResizeMode(QListWidget::Adjust);
        resourceList->setSpacing(4);
        resourceList->setGridSize(QSize(resourceIconSize + resourceList->spacing(), resourceIconSize + lineHeight));
        resourceList->setIconSize(QSize(resourceIconSize, resourceIconSize));
        resourceList->setViewMode(QListWidget::IconMode);
        resourceList->setUniformItemSizes(true);
        resourceList->setWordWrap(true);
        resourceList->setSelectionRectVisible(true);
    }

    PopulateModelList();
    rightLayout->addWidget(modelList_);
    PopulateMaterialList();
    rightLayout->addWidget(materialList_);

    QHBoxLayout* buttonRow{ new QHBoxLayout() };
    buttonRow->setContentsMargins(4, 0, 0, 0);
    materialButtons_->setLayout(new QHBoxLayout());
    materialButtons_->layout()->setMargin(0);
    buttonRow->addWidget(materialLabel_);
    buttonRow->addWidget(materialButtons_);
    buttonRow->addSpacerItem(new QSpacerItem(0, 22, QSizePolicy::Expanding, QSizePolicy::Fixed));
    rightLayout->addLayout(buttonRow);

    if (blocksIn_.Size() == 1) {

        Block* blockIn{ blocksIn_.Front() };

        for (int i{0}; i < modelList_->count(); ++i) {

            QListWidgetItem* item{ modelList_->item(i) };
            if (item->toolTip() == blockIn->modelName().CString()) {

                modelList_->setCurrentItem(item);
                break;
            }
        }

        for (int i{0}; i < materialList_->count(); ++i) {

            QListWidgetItem* item{ materialList_->item(i) };
            if (blockIn->materials().Values().Contains(item->toolTip().toStdString().data())) {

                materialList_->setCurrentItem(item);
                break;
            }
        }
    }

    QWidget* rightPanel{ new QWidget() };
    rightPanel->setLayout(rightLayout);
    splitter_->addWidget(rightPanel);

    for (int i: {0, 1}) {

        splitter_->setCollapsible  (i, false);
        splitter_->setStretchFactor(i, i + 1);
    }

    QVBoxLayout* mainLayout{ new QVBoxLayout(this) };
    mainLayout->setMargin(2);
    mainLayout->addWidget(splitter_);
    mainLayout->addWidget(buttonBox_);

    updateOkButton();
    updateMaterialButtons();

    connect(previewList_,  SIGNAL(itemSelectionChanged()), this, SLOT(updateMaterialButtons()));
    connect(modelList_,    SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedModelChanged()));
    connect(materialList_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedMaterialChanged()));

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    resize(Clamp(2 + (static_cast<int>(blocksIn_.Size()) / 5 + modelList_->count() / 17 + materialList_->count() / 17), 5, 7) * QGuiApplication::primaryScreen()->size() / 8);
    exec();
}
void BlockDialog::updateMaterialButtons()
{
    unsigned selected{ SINGLE };

    for (auto widget: materialButtons_->findChildren<QWidget*>()) {

        QRadioButton* button{ qobject_cast<QRadioButton*>(widget) };
        if (button->isChecked())
            selected = button->property("index").toUInt();

        delete button;
    }

    int geometries{};

    if (blocksIn_.Size() > 1) {
        for (QListWidgetItem* item: previewList_->selectedItems()) {

            Block* previewBlock{ qobject_cast<Block*>(qvariant_cast<QObject*>(item->data(PointerRole))) };

            if (previewBlock->numGeometries() > geometries)
                geometries = previewBlock->numGeometries();
        }
    } else {

        geometries = previewBlocks_.Front()->numGeometries();
    }

    if (geometries > 1) {

        materialLabel_->setVisible(true);

        for (int b{ -1 }; b < geometries; ++b) {

            QRadioButton* button{ new QRadioButton() };
            if (b == -1) {

                button->setText("All");
                button->setProperty("index", SINGLE);
                button->setChecked(selected == SINGLE);

            } else {

                button->setText(QString::number(b));
                button->setProperty("index", static_cast<unsigned>(b));
                if (selected == b)
                    button->setChecked(true);
            }
            materialButtons_->layout()->addWidget(button);
        }
    } else {

        materialLabel_->setVisible(false);
    }
}
void BlockDialog::PopulateModelList()
{
    PODVector<Model*> models{};
    GetSubsystem<ResourceCache>()->GetResources<Model>(models);
    for (Model* m: models) {

        Block* modelBlock{ new Block(context_) };
        modelBlock->SetModel(m);
        modelBlock->SetMaterial(GetSubsystem<ResourceCache>()->GetTempResource<Material>("Materials/None.xml"));

        BlockListItem* item{ new BlockListItem(context_, modelBlock)};
        item->setText(m->GetName().Split('/').Back().Split('.').Front().CString());
        item->setToolTip(m->GetName().CString());
        item->setData(PointerRole, QVariant(QMetaType::QObjectStar, &m));
        modelList_->addItem(item);
    }
}
void BlockDialog::PopulateMaterialList()
{
    PODVector<Material*> materials{};
    GetSubsystem<ResourceCache>()->GetResources<Material>(materials);
    for (Material* m: materials) {

        Block* materialBlock{ new Block(context_) };
        materialBlock->SetModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>("Models/MaterialBox.mdl"));
        materialBlock->SetMaterial(m);
        BlockListItem* item{ new BlockListItem(context_, materialBlock)};
        item->setText(m->GetName().Split('/').Back().Split('.').Front().CString());
        item->setToolTip(m->GetName().CString());
        item->setData(PointerRole, QVariant(QMetaType::QObjectStar, &m));
        materialList_->addItem(item);
    }
}
void BlockDialog::showEvent(QShowEvent* e)
{
    QDialog::showEvent(e);

    int size{ 16 + 130 * (1 + (static_cast<int>(blocksIn_.Size()) - 1) / Max(1, (height() - 64) / 144)) };
    splitter_->setSizes({ size, splitter_->sizes()[0] + splitter_->sizes()[1] - size });

}

void BlockDialog::accept()
{
    Blockset* currentBlockset{ GetSubsystem<EditMaster>()->GetCurrentBlockset() };
    bool modifiedSet{ false };

    //New block
    if (!blocksIn_.Size()) {

        String modelName{ modelList_->currentItem()->toolTip().toStdString().data() };
        String blockName { modelName.Split('/').Back().Split('.').Front() };
        String materialName{};
        if (materialList_->currentItem())
            materialName = materialList_->currentItem()->toolTip().toStdString().data();
        currentBlockset->AddBlock(blockName, modelName, previewBlocks_.Front()->materials());
        modifiedSet = true;

    //Modify blocks
    } else {

        for (unsigned i{0}; i < blocksIn_.Size(); ++i) {

            Block* inBlock{ blocksIn_.At(i) };
            Block* previewBlock{ previewBlocks_.At(i) };

            bool modifiedBlock{ false };
            String modelName{ previewBlock->modelName() };
            String blockName { modelName.Split('/').Back().Split('.').Front() };
            Materials materials{ previewBlock->materials() };

            if (inBlock->name() != blockName) {

                inBlock->SetName(blockName);
                modifiedBlock = true;
            }
            if (inBlock->modelName() != modelName) {

                inBlock->SetModel(previewBlock->model());
                modifiedBlock = true;
            }
            if (inBlock->materials() != materials) {

                inBlock->SetMaterials(materials);
                modifiedBlock = true;
            }

            if (modifiedBlock) {

                inBlock->SendEvent(E_BLOCKCHANGED);
                modifiedSet = true;
            }
        }

    }

    if (modifiedSet) {

        currentBlockset->blocksChanged_ = true;
        MainWindow::sidePanel()->blocksPanel()->updateBlocksetList();
        MainWindow::sidePanel()->blocksPanel()->updateBlockList();
    }

    for (Block* b: previewBlocks_)
        delete b;

    QDialog::accept();

}

void BlockDialog::selectedModelChanged()
{
    String modelName{ modelList_->currentItem()->toolTip().toStdString().data() };

    for (BlockListItem* item: previewItems_) {

        if (previewList_->selectedItems().contains(item) || blocksIn_.Size() < 2) {

            Block* previewBlock{ qobject_cast<Block*>(qvariant_cast<QObject*>(item->data(PointerRole))) };
            previewBlock->SetModel(GetSubsystem<ResourceCache>()->GetResource<Model>(modelName));
            item->UpdateBlockPreview();
        }
    }

    updateOkButton();
    updateMaterialButtons();
}
void BlockDialog::selectedMaterialChanged()
{
    String materialName{ materialList_->currentItem()->toolTip().toStdString().data() };

    for (BlockListItem* item: previewItems_) {

        if (previewList_->selectedItems().contains(item) || blocksIn_.Size() < 2) {

            Block* previewBlock{ qobject_cast<Block*>(qvariant_cast<QObject*>(item->data(PointerRole))) };

            unsigned index{ SINGLE };

            for (auto widget: materialButtons_->findChildren<QWidget*>()) {

                QRadioButton* button{ qobject_cast<QRadioButton*>(widget) };
                if (button->isChecked())
                    index = button->property("index").toUInt();
            }

            previewBlock->SetMaterial(index, GetSubsystem<ResourceCache>()->GetResource<Material>(materialName));
            item->UpdateBlockPreview();
        }
    }

    updateOkButton();
}
void BlockDialog::updateOkButton()
{
    if (!blocksIn_.Size())
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(modelList_->currentItem());
}
