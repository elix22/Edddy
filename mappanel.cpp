/* Edddy
// Copyright (C) 2019 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>

#include "editmaster.h"
#include "blockmap.h"
#include "mappanel.h"

MapPanel::MapPanel(Context* context, QWidget *parent) : QWidget(parent), Object(context)
{
    QVBoxLayout* mainLayout{ new QVBoxLayout() };

    QHBoxLayout* mapNameRow{ new QHBoxLayout() };
    QLabel* mapNameLabel{ new QLabel("Name:") };
    nameEdit_ = new QLineEdit();
    QPushButton* resetMapNameButton{ new QPushButton() };
    resetMapNameButton->setIcon(QIcon(":/Undo"));
    resetMapNameButton->setToolTip("Restore map name");

    mapNameRow->addWidget(mapNameLabel);
    mapNameRow->addWidget(nameEdit_);
    mapNameRow->addWidget(resetMapNameButton);
    mainLayout->addLayout(mapNameRow);

    QFrame* firstLine{ new QFrame() };
    firstLine->setFrameShape(QFrame::HLine);
    firstLine->setFrameShadow(QFrame::Sunken);
    mainLayout->addWidget(firstLine);

    QHBoxLayout* mapSizeRow{ new QHBoxLayout() };
    for (int i{0}; i < 4; ++i) {

        QVBoxLayout* dimensionColumn{ new QVBoxLayout() };

        if (i != 3) {

            QLabel* dimensionLabel{ new QLabel() };
            dimensionLabel->setAlignment(Qt::AlignHCenter);
            QSpinBox* dimensionBox{ new QSpinBox() };
            dimensionBox->setReadOnly(true);
            dimensionBox->setButtonSymbols(QSpinBox::NoButtons);
            dimensionBox->setAlignment(Qt::AlignHCenter);

            switch (i) {
            case 0:
                dimensionLabel->setText("Width");
                widthBox_ = dimensionBox;
                break;
            case 1:
                dimensionLabel->setText("Height");
                heightBox_ = dimensionBox;
                break;
            case 2:
                dimensionLabel->setText("Depth");
                depthBox_ = dimensionBox;
                break;
            default:
                break;
            }

            dimensionColumn->addWidget(dimensionLabel);
            dimensionColumn->addWidget(dimensionBox);

        } else {

            QPushButton* resizeButton{ new QPushButton() };
            resizeButton->setIcon(QIcon(":/Edit"));
            resizeButton->setToolTip("Resize map");
            dimensionColumn->addWidget(resizeButton);
            dimensionColumn->setAlignment(resizeButton, Qt::AlignBottom);
        }

        mapSizeRow->addLayout(dimensionColumn);
        mapSizeRow->setStretch(i, i != 3);
    }
    mainLayout->addLayout(mapSizeRow);

    QFrame* secondLine{ new QFrame() };
    secondLine->setFrameShape(QFrame::HLine);
    secondLine->setFrameShadow(QFrame::Sunken);
    mainLayout->addWidget(secondLine);

    QHBoxLayout* mapBackgroundRow{ new QHBoxLayout() };
    QLabel* backgroundLabel{ new QLabel("Background:") };
    QComboBox* backgroundBox{ new QComboBox() };
    backgroundBox->addItems({"Color", "CubeMap"});
    backgroundBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    mapBackgroundRow->addWidget(backgroundLabel);
    mapBackgroundRow->addWidget(backgroundBox);
    mapBackgroundRow->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum) );
    mainLayout->addLayout(mapBackgroundRow);

    QHBoxLayout* colorRow{ new QHBoxLayout() };
    QFrame* colorFrame{ new QFrame() };
    colorFrame->setFixedSize(64, 64);
    colorFrame->setFrameShape(QFrame::Box);
    colorFrame->setStyleSheet("background-color: " + QColor::fromRgb(0, 0, 0).name() + "; border: 2px inset #333;");

    QVBoxLayout* colorColumn{ new QVBoxLayout() };
    for (int c{0}; c <3; ++c) {

        QHBoxLayout* sliderRow{ new QHBoxLayout };

        QLabel* colorLabel{ new QLabel() };

        switch (c) {
        case 0: colorLabel->setText("R"); break;
        case 1: colorLabel->setText("G"); break;
        case 2: colorLabel->setText("B"); break;
        default: break;
        }

        QSlider* colorSlider{ new QSlider() };
        colorSlider->setOrientation(Qt::Horizontal);

        sliderRow->addWidget(colorLabel);
        sliderRow->addWidget(colorSlider);
        colorColumn->addLayout(sliderRow);
    }

    colorRow->addWidget(colorFrame);
    colorRow->addLayout(colorColumn);
    mainLayout->addLayout(colorRow);

    mainLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding) );
    setLayout(mainLayout);

    setEnabled(false);
}
void MapPanel::updateInfo()
{
    BlockMap* activeBlockMap{ GetSubsystem<EditMaster>()->GetActiveBlockMap() };

    if (activeBlockMap) {

        nameEdit_->setText(  activeBlockMap->GetName().CString());
        widthBox_->setValue( activeBlockMap->GetMapWidth());
        heightBox_->setValue(activeBlockMap->GetMapHeight());
        depthBox_->setValue( activeBlockMap->GetMapDepth());

        setEnabled(true);

    } else {

        nameEdit_->clear();
        widthBox_->setValue(0);
        heightBox_->setValue(0);
        depthBox_->setValue(0);

        setEnabled(false);
    }
}
